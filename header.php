<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> <?php storefront_html_tag_schema(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>?ver=1.0.25" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/printshop-top-bar.css" type="text/css" media="screen" />
	<?php
	global $options;
	foreach ($options as $value) {
		if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); }
	}
	?>	
	<?php switch ($mst_style_sheet) {
		 case "default":?>
		<?php break; ?>
	<?php case "green":?>
		<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/green.css" type="text/css" media="screen" />
	<?php break; ?>
	<?php case "blue":?>
		<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/blue.css" type="text/css" media="screen" />
	<?php break; ?>
	<?php case "red":?>
		<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/red.css" type="text/css" media="screen" />
	<?php break; ?>
	<?php }?>

	<?php wp_head(); ?>
</head>
<?php $site_background_style = apply_filters( 'printshop_background_color', $site_background_style, 'background' ); ?>

<body <?php body_class(); ?><?php if ( $site_background_style ) { echo 'style="'.$site_background_style.'"'; } ?>>
<div id="page" class="hfeed site">
	<?php
	do_action( 'storefront_before_header' );
	
	$site_header_style = '';
	if ( get_header_image() != '' ) {
		$site_header_style = 'background-image: url(' . esc_url( get_header_image() ) . ');';
	}
	$site_header_style = apply_filters( 'printshop_background_color', $site_header_style, 'logomenu' );
	?>

	<header id="masthead" class="site-header" role="banner" <?php if ( $site_header_style ) { echo 'style="'.$site_header_style.'"'; } ?>>
		<div class="col-full">

			<?php
			/**
			 * @hooked storefront_skip_links - 0
			 * @hooked storefront_social_icons - 10
			 * @hooked storefront_site_branding - 20
			 * @hooked storefront_secondary_navigation - 30
			 * @hooked storefront_product_search - 40
			 * @hooked storefront_primary_navigation - 50
			 * @hooked storefront_header_cart - 60
			 */
			do_action( 'storefront_header' ); ?>

		</div>
	</header><!-- #masthead -->	

	<?php
	printshop_header_slider();

	/**
	 * @hooked storefront_header_widget_region - 10
	 */
	do_action( 'storefront_before_content' ); ?>

	<?php $site_content_style = apply_filters( 'printshop_background_color', $site_content_style, 'content' ); ?>
	<div id="content" class="site-content" tabindex="-1"<?php if ( $site_content_style ) { echo 'style="'.$site_content_style.'"'; } ?>>
		<div class="col-full">

		<?php
		/**
		 * @hooked woocommerce_breadcrumb - 10
		 */
		do_action( 'storefront_content_top' ); ?>		
