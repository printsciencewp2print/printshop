<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */
?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' );
	$site_footer_style = apply_filters( 'printshop_background_color', $site_footer_style, 'footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo"<?php if ( $site_footer_style ) { echo ' style="'.$site_footer_style.'"'; } ?>>
		<div class="col-full">

			<?php
			/**
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit - 20
			 */
			do_action( 'storefront_footer' ); ?>

		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

<?php if (is_single()) { ?>
	<script>
	if (jQuery('.woocommerce-product-gallery').size()) {
		setTimeout(function(){
			var pdhtml = jQuery('.product-description').html();
			jQuery('.product-description').remove();
			jQuery('.woocommerce-product-gallery').append('<div class="product-description">'+pdhtml+'</div>');
		}, 1000);
	}
	</script>
<?php } ?>

</body>
</html>
