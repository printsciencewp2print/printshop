<?php
add_action('admin_menu', 'slideshow_options_add_options_page');
function slideshow_options_add_options_page() {
	add_theme_page(
		__('Slideshow Options'),
		__('Slideshow Options'),
		10,
		'slideshow-options',
		'fslideshow_options_page'
	);
}

function fslideshow_options_page() {
	if ($_POST['slideshow_options_form_submit'] == 'true') {
		update_option("slideshow_options", $_POST['slideshow_options']);
	}
	$page_on_front = get_option('page_on_front');
	$slideshow_options = get_option("slideshow_options");

	$wppages = get_pages('exclude='.$page_on_front);
	$wpcats = get_terms('category', 'hide_empty=0');
	$prodcats = get_terms('product_cat', 'hide_empty=0');
	$slide_groups = get_terms('slide-page', 'hide_empty=0');

	$wppage_names = array();
	$wpterm_names = array();
	$slide_group_names = array();
	if ($wppages) { foreach($wppages as $wppage) { $wppage_names[$wppage->ID] = $wppage->post_title; } }
	if ($wpcats) { foreach($wpcats as $wpcat) { $wpterm_names[$wpcat->term_id] = $wpcat->name; } }
	if ($prodcats) { foreach($prodcats as $prodcat) { $wpterm_names[$prodcat->term_id] = $prodcat->name; } }
	if ($slide_groups) { foreach($slide_groups as $slide_group) { $slide_group_names[$slide_group->slug] = $slide_group->name; } }
?>
	<div class="wrap">
		<?php screen_icon(); ?>
		<h2><?php echo __('Slideshow Options'); ?></h2><br>
		<?php if ($slide_groups) { ?>
			<form class="slideshow-options-form" method="POST">
			<input type="hidden" name="slideshow_options_form_submit" value="true">
			<?php if($_POST['slideshow_options_form_submit'] == 'true') { ?><div id="message" class="updated fade"><p><?php _e('Options Saved.'); ?></p></div><?php } ?>
			<div style="border-top:1px solid #C1C1C1; border-bottom:1px solid #C1C1C1; margin-bottom:15px; padding:15px 0;">
				<strong><?php echo __('CONFIGURED SLIDESHOW LIST'); ?>:</strong><br><br>
				<?php $lastsotype = ''; ?>
				<?php foreach($slideshow_options as $so_key => $so_val) {
					if ($so_val) {
						if (substr($so_key, 0, 4) == 'page') { $sotype = 'page'; } else { $sotype = 'cat'; }
						if ($lastsotype != '' && $sotype != $lastsotype) { echo '<br>'; }
						if ($sotype == 'page') {
							$pid = str_replace('page-', '', $so_key);
							if ($pid == 'home') {
								echo 'Homepage - '.$slide_group_names[$so_val].'<br>';
							} else {
								echo $wppage_names[$pid].' - '.$slide_group_names[$so_val].'<br>';
							}
						} else {
							$cid = (int)str_replace('cat-', '', $so_key);
							echo $wpterm_names[$cid].' - '.$slide_group_names[$so_val].'<br>';
						}
						$lastsotype = $sotype;
					}
				} ?>
			</div>
			<table style="width:auto;">
				<?php if ($wppages) { ?>
				  <tr>
					<td><?php _e('Page'); ?>:&nbsp;</td>
					<td class="page-obj">
						<select name="wp_page" onchange="soptions_show_slides('page')" style="width:100%;">
							<option value="">-- <?php _e('Select page'); ?> --</option>
							<option value="home"><?php _e('Homepage'); ?></option>
							<?php foreach($wppages as $wppage) { ?>
								<option value="<?php echo $wppage->ID; ?>"><?php if ($wppage->post_parent) { echo '&nbsp;&nbsp;&nbsp;&nbsp;'; } ?><?php echo $wppage->post_title; ?></option>
							<?php } ?>
						</select>
					</td>
					<td class="page-sopt">
						<select name="slideshow_options[page-home]" class="sopt-page-home" style="display:none;">
							<option value="">-- <?php _e('Select slideshow'); ?> --</option>
							<?php foreach($slide_groups as $slide_group) { $s = ''; if ($slideshow_options['page-home'] == $slide_group->slug) { $s = ' SELECTED'; } ?>
								<option value="<?php echo $slide_group->slug; ?>"<?php echo $s; ?>><?php echo $slide_group->name; ?></option>
							<?php } ?>
						</select>
						<?php foreach($wppages as $wppage) { ?>
							<select name="slideshow_options[page-<?php echo $wppage->ID; ?>]" class="sopt-page-<?php echo $wppage->ID; ?>" style="display:none;">
								<option value="">-- <?php _e('Select slideshow'); ?> --</option>
								<?php foreach($slide_groups as $slide_group) { $s = ''; if ($slideshow_options['page-'.$wppage->ID] == $slide_group->slug) { $s = ' SELECTED'; } ?>
									<option value="<?php echo $slide_group->slug; ?>"<?php echo $s; ?>><?php echo $slide_group->name; ?></option>
								<?php } ?>
							</select>
						<?php } ?>
					</td>
				  </tr>
				<?php } ?>
				<?php if ($wpcats) { ?>
				  <tr>
					<td><?php _e('Post Category'); ?>:&nbsp;</td>
					<td class="cat-obj">
						<select name="wp_cat" onchange="soptions_show_slides('cat')" style="width:100%;">
							<option value="">-- <?php _e('Select category'); ?> --</option>
							<?php foreach($wpcats as $wpcat) { ?>
								<option value="<?php echo $wpcat->term_id; ?>"><?php echo $wpcat->name; ?></option>
							<?php } ?>
						</select>
					</td>
					<td class="cat-sopt">
						<?php foreach($wpcats as $wpcat) { ?>
							<select name="slideshow_options[cat-<?php echo $wpcat->term_id; ?>]" class="sopt-cat-<?php echo $wpcat->term_id; ?>" style="display:none;">
								<option value="">-- <?php _e('Select slideshow'); ?> --</option>
								<?php foreach($slide_groups as $slide_group) { $s = ''; if ($slideshow_options['cat-'.$wpcat->term_id] == $slide_group->slug) { $s = ' SELECTED'; } ?>
									<option value="<?php echo $slide_group->slug; ?>"<?php echo $s; ?>><?php echo $slide_group->name; ?></option>
								<?php } ?>
							</select>
						<?php } ?>
					</td>
				  </tr>
				<?php } ?>
				<?php if ($prodcats) { ?>
				  <tr>
					<td><?php _e('Product Category'); ?>:&nbsp;</td>
					<td class="prodcat-obj">
						<select name="prod_cat" onchange="soptions_show_slides('prodcat')" style="width:100%;">
							<option value="">-- <?php _e('Select category'); ?> --</option>
							<?php foreach($prodcats as $prodcat) { ?>
								<option value="<?php echo $prodcat->term_id; ?>"><?php echo $prodcat->name; ?></option>
							<?php } ?>
						</select>
					</td>
					<td class="prodcat-sopt">
						<?php foreach($prodcats as $prodcat) { ?>
							<select name="slideshow_options[cat-<?php echo $prodcat->term_id; ?>]" class="sopt-prodcat-<?php echo $prodcat->term_id; ?>" style="display:none;">
								<option value="">-- <?php _e('Select slideshow'); ?> --</option>
								<?php foreach($slide_groups as $slide_group) { $s = ''; if ($slideshow_options['cat-'.$prodcat->term_id] == $slide_group->slug) { $s = ' SELECTED'; } ?>
									<option value="<?php echo $slide_group->slug; ?>"<?php echo $s; ?>><?php echo $slide_group->name; ?></option>
								<?php } ?>
							</select>
						<?php } ?>
					</td>
				  </tr>
				<?php } ?>
			</table>
			<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save Options') ?>" /></p>
			</form>
			<script>
			function soptions_show_slides(o) {
				var oid = jQuery('.slideshow-options-form .'+o+'-obj select').val();
				jQuery('.slideshow-options-form .'+o+'-sopt select').hide();
				if (oid) {
					jQuery('.slideshow-options-form .'+o+'-sopt select.sopt-'+o+'-'+oid).fadeIn();
				}
			}
			</script>
		<?php } else { ?>
			<p><?php _e('No Slide Groups.'); ?></p>
		<?php } ?>
	</div>
<?php
}
?>