<?php
add_action('admin_head', 'my_custom_logo');
function my_custom_logo() {
	?>
	<style type="text/css">
	div#wpadminbar #wp-admin-bar-wp-logo>.ab-item .ab-icon:before{
	content:'';
	display:inline-block;
	vertical-align: top;
	background: url(<?php echo get_bloginfo('template_directory'); ?>/images/custom-logo.png) no-repeat;
	width: 20px;
	height: 20px;
	-webkit-background-size: contain;
	background-size: contain;
	}
	</style>
	<?php
}
?>