<?php
add_action('wp_loaded', 'printshop_after_setup_theme', 100);
function printshop_after_setup_theme() {
	global $shop_page_id, $cart_page_id;

	$printshop_installed = get_option('printshop_installed');
	if ($printshop_installed) { return; }
	// ---------------------------------------------------------------------------------
	// Woocommerce Pages
	// ---------------------------------------------------------------------------------
	$woo_shop_page = get_page_by_path('shop');
	if ($woo_shop_page) {
		$shop_page_id = $woo_shop_page->ID;
	} else {
		$new_page = array();
		$new_page['post_title'] = __('Shop', 'printshop');
		$new_page['post_content'] = '';
		$new_page['post_status'] = 'publish';
		$new_page['post_type'] = 'page';
		$new_page['menu_order'] = 0;
		$shop_page_id = wp_insert_post($new_page);
		update_option('woocommerce_shop_page_id', $shop_page_id);
	}
	update_option('page_on_front', $shop_page_id);
	update_option('show_on_front', 'page');

	$woo_cart_page = get_page_by_path('cart');
	if ($woo_cart_page) {
		$cart_page_id = $woo_cart_page->ID;
	} else {
		$new_page = array();
		$new_page['post_title'] = __('Cart', 'printshop');
		$new_page['post_content'] = '[woocommerce_cart]';
		$new_page['post_status'] = 'publish';
		$new_page['post_type'] = 'page';
		$new_page['menu_order'] = 0;
		$cart_page_id = wp_insert_post($new_page);
		update_option('woocommerce_cart_page_id', $cart_page_id);
	}

	$woo_checkout_page = get_page_by_path('checkout');
	if (!$woo_checkout_page) {
		$new_page = array();
		$new_page['post_title'] = __('Checkout', 'printshop');
		$new_page['post_content'] = '[woocommerce_checkout]';
		$new_page['post_status'] = 'publish';
		$new_page['post_type'] = 'page';
		$new_page['menu_order'] = 0;
		$checkout_page_id = wp_insert_post($new_page);
		update_option('woocommerce_checkout_page_id', $checkout_page_id);
	}

	$woo_myaccount_page = get_page_by_path('my-account');
	if (!$woo_myaccount_page) {
		$new_page = array();
		$new_page['post_title'] = __('My Account', 'printshop');
		$new_page['post_content'] = '[woocommerce_my_account]';
		$new_page['post_status'] = 'publish';
		$new_page['post_type'] = 'page';
		$new_page['menu_order'] = 0;
		$myaccount_page_id = wp_insert_post($new_page);
		update_option('woocommerce_myaccount_page_id', $myaccount_page_id);
	}

	// ---------------------------------------------------------------------------------
	// Woocommerce Settings
	// ---------------------------------------------------------------------------------
	update_option('woocommerce_cart_redirect_after_add', 'yes');

	// ---------------------------------------------------------------------------------
	// Site Menus
	// ---------------------------------------------------------------------------------
	$main_menu = printshop_get_main_menu();
	$footer_menu = printshop_get_footer_menu();
	$login_menu = printshop_get_top_bar_login_menu();

	// ---------------------------------------------------------------------------------
	// Header and Footer Widgets
	// ---------------------------------------------------------------------------------
	$sidebars_widgets = get_option('sidebars_widgets');

	// ---------------------------
	// Top Bar 1
	// ---------------------------
	// text widget 'Optional Text'
	$widget_text = get_option('widget_text');
	if (!is_array($widget_text)) { $widget_text = array(); }
	$twcount = count($widget_text) + 1;
	$widget_text[$twcount] = array('title' => '', 'text' => __('Optional Text', 'printshop'), 'filter' => false);
	update_option('widget_text', $widget_text);

	$sidebars_widgets['woa-top-bar-1'] = array('text-'.$twcount);

	// ---------------------------
	// Top Bar 2
	// ---------------------------
	// woocommerce_product_search widget
	$widget_woocommerce_product_search = get_option('widget_woocommerce_product_search');
	if (!is_array($widget_woocommerce_product_search)) { $widget_woocommerce_product_search = array(); }
	$wpscount = count($widget_woocommerce_product_search) + 1;
	$widget_woocommerce_product_search[$wpscount] = array('title' => '');
	update_option('widget_woocommerce_product_search', $widget_woocommerce_product_search);

	// nav_menu widget
	$widget_nav_menu = get_option('widget_nav_menu');
	if (!is_array($widget_nav_menu)) { $widget_nav_menu = array(); }
	$wnmcount = count($widget_nav_menu) + 1;
	$widget_nav_menu[$wnmcount] = array('title' => '', 'nav_menu' => $login_menu);
	update_option('widget_nav_menu', $widget_nav_menu);

	// text widget 'cart icon'
	$cart_url = home_url('/') . 'cart/';
	$widget_text = get_option('widget_text');
	if (!is_array($widget_text)) { $widget_text = array(); }
	$twcount = count($widget_text) + 1;
	$widget_text[$twcount] = array('title' => '', 'text' => '<a title="'.__('View your shopping cart', 'printshop').'" href="'.$cart_url.'" class="cart-contents"></a>', 'filter' => false);
	update_option('widget_text', $widget_text);

	$sidebars_widgets['woa-top-bar-2'] = array('woocommerce_product_search-'.$wpscount, 'nav_menu-'.$wnmcount, 'text-'.$twcount);

	// ---------------------------
	// footer-1
	// ---------------------------
	$widget_designmodo_social_profile = get_option('widget_designmodo_social_profile');
	if (!is_array($designmodo_social_profile)) { $designmodo_social_profile = array(); }
	$dspcount = count($designmodo_social_profile) + 1;
	$widget_designmodo_social_profile[$dspcount] = array('title' => __('My Social Profile', 'printshop'), 'facebook' => '#', 'twitter' => '#', 'google' => '#', 'linkedin' => '#');
	update_option('widget_designmodo_social_profile', $widget_designmodo_social_profile);

	$sidebars_widgets['footer-1'] = array('designmodo_social_profile-'.$dspcount);

	// ---------------------------
	// footer-2
	// ---------------------------
	// text widget 'Order Online or Call'
	$widget_text = get_option('widget_text');
	if (!is_array($widget_text)) { $widget_text = array(); }
	$twcount = count($widget_text) + 1;
	$widget_text[$twcount] = array('title' => __('Order Online or Call', 'printshop'), 'text' => '0800 000 0000', 'filter' => false);
	update_option('widget_text', $widget_text);

	$sidebars_widgets['footer-2'] = array('text-'.$twcount);

	// ---------------------------
	// footer-3
	// ---------------------------
	// nav_menu widget
	$widget_nav_menu = get_option('widget_nav_menu');
	if (!is_array($widget_nav_menu)) { $widget_nav_menu = array(); }
	$wnmcount = count($widget_nav_menu) + 1;
	$widget_nav_menu[$wnmcount] = array('title' => __('Footer Menu', 'printshop'), 'nav_menu' => $footer_menu);
	update_option('widget_nav_menu', $widget_nav_menu);

	$sidebars_widgets['footer-3'] = array('nav_menu-'.$wnmcount);

	// clear default sidebar
	$sidebars_widgets['header-1'] = array();
	$sidebars_widgets['sidebar-1'] = array();
	$sidebars_widgets['footer-4'] = array();

	update_option('sidebars_widgets', $sidebars_widgets);
	update_option('sidebars_widgets2', $sidebars_widgets);
	update_option('printshop_installed', '1');

	// copy translation files
	$themes_lang_folder = $_SERVER['DOCUMENT_ROOT'].'/wp-content/languages/themes';
	if (is_dir($themes_lang_folder)) {
		$langpofiles = glob(dirname(__FILE__).'/../../languages/*.po');
		if (count($langpofiles)) {
			foreach($langpofiles as $langpofile) {
				@copy($langpofile, $themes_lang_folder.'/'.basename($langpofile));
			}
		}
		$langmofiles = glob(dirname(__FILE__).'/../../languages/*.mo');
		if (count($langmofiles)) {
			foreach($langmofiles as $langmofile) {
				@copy($langmofile, $themes_lang_folder.'/'.basename($langmofile));
			}
		}
	}
}

function printshop_get_main_menu() {
	global $wpdb, $shop_page_id, $cart_page_id;

	$main_nav_menu = $wpdb->get_var(sprintf("SELECT t.term_id FROM %sterms t LEFT JOIN %sterm_taxonomy tt ON tt.term_id = t.term_id WHERE t.slug = 'main-menu' AND tt.taxonomy = 'nav_menu'", $wpdb->prefix, $wpdb->prefix));
	if (!$main_nav_menu) {
		// terms
		$insert = array();
		$insert['name'] = 'Main Menu';
		$insert['slug'] = 'main-menu';
		$insert['term_group'] = 0;
		$wpdb->insert($wpdb->prefix."terms", $insert);
		$term_id = $wpdb->insert_id;

		$main_nav_menu = $term_id;

		// term taxonomy
		$insert = array();
		$insert['term_id'] = $term_id;
		$insert['taxonomy'] = 'nav_menu';
		$insert['description'] = '';
		$insert['parent'] = 0;
		$insert['count'] = 1;
		$wpdb->insert($wpdb->prefix."term_taxonomy", $insert);
		$term_taxonomy_id = $wpdb->insert_id;

		$locations = get_theme_mod('nav_menu_locations');
		$locations['primary'] = $main_nav_menu;
		set_theme_mod('nav_menu_locations', $locations);

		$morder = 1;
		// add Home item
		$nmi_post = array();
		$nmi_post['post_title'] = __('Home', 'printshop');
		$nmi_post['post_content'] = '';
		$nmi_post['post_status'] = 'publish';
		$nmi_post['post_type'] = 'nav_menu_item';
		$nmi_post['menu_order'] = $morder;
		$object_id = wp_insert_post($nmi_post);

		$pmetas = array('_menu_item_url' => home_url(), '_menu_item_object' => 'custom', '_menu_item_type' => 'custom', '_menu_item_object_id' => $object_id, '_menu_item_menu_item_parent' => '0');
		foreach($pmetas as $pmkey => $pmval) {
			$pmeta = array();
			$pmeta['post_id'] = $object_id;
			$pmeta['meta_key'] = $pmkey;
			$pmeta['meta_value'] = $pmval;
			$wpdb->insert($wpdb->prefix."postmeta", $pmeta);
		}
		// term relationships
		$insert = array();
		$insert['object_id'] = $object_id;
		$insert['term_taxonomy_id'] = $term_taxonomy_id;
		$insert['term_order'] = 0;
		$wpdb->insert($wpdb->prefix."term_relationships", $insert);
		$morder++;

		$mitems = array('shop' => __('Products', 'printshop'), 'cart' => __('Cart', 'printshop'), 'contact' => __('Contact', 'printshop'));
		foreach($mitems as $mikey => $mitem) {
			$wppage = get_page_by_path($mikey);
			if ($wppage) {
				$wppid = $wppage->ID;
			} else {
				$new_page = array();
				$new_page['post_title'] = $mitem;
				$new_page['post_content'] = 'Lorem ipsum dolor sit amet';
				$new_page['post_status'] = 'publish';
				$new_page['post_type'] = 'page';
				$new_page['menu_order'] = 0;
				$wppid = wp_insert_post($new_page);
			}
			$mi_post = array();
			$mi_post['post_title'] = '';
			$mi_post['post_content'] = '';
			$mi_post['post_status'] = 'publish';
			$mi_post['post_type'] = 'nav_menu_item';
			$mi_post['menu_order'] = $morder;
			$object_id = wp_insert_post($mi_post);

			$pmetas = array('_menu_item_url' => '', '_menu_item_object' => 'page', '_menu_item_type' => 'post_type', '_menu_item_object_id' => $wppid, '_menu_item_menu_item_parent' => '0');
			foreach($pmetas as $pmkey => $pmval) {
				$pmeta = array();
				$pmeta['post_id'] = $object_id;
				$pmeta['meta_key'] = $pmkey;
				$pmeta['meta_value'] = $pmval;
				$wpdb->insert($wpdb->prefix."postmeta", $pmeta);
			}

			// term relationships
			$insert = array();
			$insert['object_id'] = $object_id;
			$insert['term_taxonomy_id'] = $term_taxonomy_id;
			$insert['term_order'] = 0;
			$wpdb->insert($wpdb->prefix."term_relationships", $insert);
			$morder++;
		}
	}
}

function printshop_get_footer_menu() {
	global $wpdb;

	$footer_nav_menu = $wpdb->get_var(sprintf("SELECT t.term_id FROM %sterms t LEFT JOIN %sterm_taxonomy tt ON tt.term_id = t.term_id WHERE t.slug = 'footer-menu' AND tt.taxonomy = 'nav_menu'", $wpdb->prefix, $wpdb->prefix));
	if (!$footer_nav_menu) {
		// terms
		$insert = array();
		$insert['name'] = __('Footer Menu', 'printshop');
		$insert['slug'] = 'footer-menu';
		$insert['term_group'] = 0;
		$wpdb->insert($wpdb->prefix."terms", $insert);
		$term_id = $wpdb->insert_id;

		$footer_nav_menu = $term_id;

		// term taxonomy
		$insert = array();
		$insert['term_id'] = $term_id;
		$insert['taxonomy'] = 'nav_menu';
		$insert['description'] = '';
		$insert['parent'] = 0;
		$insert['count'] = 1;
		$wpdb->insert($wpdb->prefix."term_taxonomy", $insert);
		$term_taxonomy_id = $wpdb->insert_id;

		// add menu items
		$morder = 1;
		$mitems = array('about-us' => __('About us', 'printshop'), 'privacy-policy' => __('Privacy policy', 'printshop'), 'conditions-of-sale' => __('Conditions of sale', 'printshop'));
		foreach($mitems as $mikey => $mitem) {
			$wppage = get_page_by_path($mikey);
			if ($wppage) {
				$wppid = $wppage->ID;
			} else {
				$new_page = array();
				$new_page['post_title'] = $mitem;
				$new_page['post_content'] = 'Lorem ipsum dolor sit amet';
				$new_page['post_status'] = 'publish';
				$new_page['post_type'] = 'page';
				$new_page['menu_order'] = 0;
				$wppid = wp_insert_post($new_page);
			}
			$mi_post = array();
			$mi_post['post_title'] = '';
			$mi_post['post_content'] = '';
			$mi_post['post_status'] = 'publish';
			$mi_post['post_type'] = 'nav_menu_item';
			$mi_post['menu_order'] = $morder;
			$object_id = wp_insert_post($mi_post);

			$pmetas = array('_menu_item_url' => '', '_menu_item_object' => 'page', '_menu_item_type' => 'post_type', '_menu_item_object_id' => $wppid, '_menu_item_menu_item_parent' => '0');
			foreach($pmetas as $pmkey => $pmval) {
				$pmeta = array();
				$pmeta['post_id'] = $object_id;
				$pmeta['meta_key'] = $pmkey;
				$pmeta['meta_value'] = $pmval;
				$wpdb->insert($wpdb->prefix."postmeta", $pmeta);
			}

			// term relationships
			$insert = array();
			$insert['object_id'] = $object_id;
			$insert['term_taxonomy_id'] = $term_taxonomy_id;
			$insert['term_order'] = 0;
			$wpdb->insert($wpdb->prefix."term_relationships", $insert);
			$morder++;
		}
	}
	return $footer_nav_menu;
}

function printshop_get_top_bar_login_menu() {
	global $wpdb;

	$login_nav_menu = $wpdb->get_var(sprintf("SELECT t.term_id FROM %sterms t LEFT JOIN %sterm_taxonomy tt ON tt.term_id = t.term_id WHERE t.slug = 'login' AND tt.taxonomy = 'nav_menu'", $wpdb->prefix, $wpdb->prefix));
	if (!$login_nav_menu) {
		// terms
		$insert = array();
		$insert['name'] = 'login';
		$insert['slug'] = 'login';
		$insert['term_group'] = 0;
		$wpdb->insert($wpdb->prefix."terms", $insert);
		$term_id = $wpdb->insert_id;

		$login_nav_menu = $term_id;

		// term taxonomy
		$insert = array();
		$insert['term_id'] = $term_id;
		$insert['taxonomy'] = 'nav_menu';
		$insert['description'] = '';
		$insert['parent'] = 0;
		$insert['count'] = 1;
		$wpdb->insert($wpdb->prefix."term_taxonomy", $insert);
		$term_taxonomy_id = $wpdb->insert_id;

		// add login menu item
		$nmi_post = array();
		$nmi_post['post_title'] = __('Log In', 'printshop').'|'.__('My Account', 'printshop');
		$nmi_post['post_content'] = '';
		$nmi_post['post_status'] = 'publish';
		$nmi_post['post_type'] = 'nav_menu_item';
		$nmi_post['menu_order'] = 1;
		$object_id = wp_insert_post($nmi_post);

		$pmetas = array('_menu_item_url' => '#aiwoologinout#', '_menu_item_object' => 'custom', '_menu_item_type' => 'custom', '_menu_item_object_id' => $object_id, '_menu_item_menu_item_parent' => '0');
		foreach($pmetas as $pmkey => $pmval) {
			$pmeta = array();
			$pmeta['post_id'] = $object_id;
			$pmeta['meta_key'] = $pmkey;
			$pmeta['meta_value'] = $pmval;
			$wpdb->insert($wpdb->prefix."postmeta", $pmeta);
		}

		// term relationships
		$insert = array();
		$insert['object_id'] = $object_id;
		$insert['term_taxonomy_id'] = $term_taxonomy_id;
		$insert['term_order'] = 0;
		$wpdb->insert($wpdb->prefix."term_relationships", $insert);
	}
	return $login_nav_menu;
}
?>