<?php
/**
 * storefront engine room
 *
 * @package storefront
 */

/**
 * Setup.
 * Enqueue styles, register widget regions, etc.
 */
require get_template_directory() . '/inc/functions/setup.php';
require get_template_directory() . '/inc/functions/install.php';
require get_template_directory() . '/inc/functions/shortcodes.php';
require get_template_directory() . '/inc/functions/widgets.php';


/**
 * Structure.
 * Template functions used throughout the theme.
 */
require get_template_directory() . '/inc/structure/hooks.php';
require get_template_directory() . '/inc/structure/post.php';
require get_template_directory() . '/inc/structure/page.php';
require get_template_directory() . '/inc/structure/header.php';
require get_template_directory() . '/inc/structure/footer.php';
require get_template_directory() . '/inc/structure/comments.php';
require get_template_directory() . '/inc/structure/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/functions/extras.php';

/**
 * Customizer additions.
 */
if ( is_storefront_customizer_enabled() ) {
	require get_template_directory() . '/inc/customizer/hooks.php';
	require get_template_directory() . '/inc/customizer/controls.php';
	require get_template_directory() . '/inc/customizer/display.php';
	require get_template_directory() . '/inc/customizer/functions.php';
	require get_template_directory() . '/inc/customizer/custom-header.php';
}

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack/hooks.php';
require get_template_directory() . '/inc/jetpack/functions.php';

/**
 * Welcome screen
 */
if ( is_admin() ) {
	require get_template_directory() . '/inc/admin/admin.php';
	require get_template_directory() . '/inc/admin/slideshow-options.php';
	//require // get_template_directory() . '/inc/admin/welcome-screen/welcome-screen.php';
}

/**
 * Load WooCommerce compatibility files.
 */
if ( is_woocommerce_activated() ) {
	require get_template_directory() . '/inc/woocommerce/hooks.php';
	require get_template_directory() . '/inc/woocommerce/functions.php';
	require get_template_directory() . '/inc/woocommerce/template-tags.php';
	require get_template_directory() . '/inc/woocommerce/integrations.php';
}
function remove_core_updates(){
global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}

add_filter('pre_site_transient_update_themes','remove_core_updates'); 

function printshop_before_single_product_summary() {
	?>
	<div class="product-description" style="display:none;">
		<?php the_content(); ?>
	</div>
	<?php
}
add_action( 'woocommerce_before_single_product_summary', 'printshop_before_single_product_summary', 100 );


add_filter( 'get_product_search_form' , 'woo_custom_product_searchform' );

/**
 * woo_custom_product_searchform
 *
 * @access      public
 * @since       1.0 
 * @return      void
*/
function woo_custom_product_searchform( $form ) {
	
	$form = '<form role="search" method="get" id="searchform" action="' . esc_url( home_url( '/'  ) ) . '">
		<div>
			<label class="screen-reader-text" for="s">' . __( 'Search', 'printshop' ) . ':</label>
			<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . __( 'Search Products', 'printshop' ) . '&hellip;" />
			<input type="submit" id="searchsubmit" value="'. esc_attr__( 'Search', 'printshop' ) .'" />
			<input type="hidden" name="post_type" value="product" />
		</div>
	</form>';
	
	return $form;
	
}

add_filter( 'woocommerce_enable_setup_wizard', 'printshop_woo_enable_setup_wizard' );
function printshop_woo_enable_setup_wizard($enable_setup_wizard) {
	return false;
}
?>