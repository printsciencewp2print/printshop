��    i      d  �   �       	  
   	     	     #	     4	     D	     Z	  	   ]	     g	     �	  !   �	     �	     �	  
   �	     �	     �	      
     
     
     
     #
     6
     K
     ^
     f
  3   z
  2   �
     �
     �
  
   �
     �
            	        $     0  
   6     A     P     l     z     �     �     �     �  
   �      �  W   �     9     H     O     _  
   s     ~     �     �     �  
   �     �  
   �     �     �     �                 %   %     K     Y     n     u     �     �     �     �     �     �  F   �     )     5     E     L     \     s     �     �     �     �     �  \   �  
   $     /  
   6  �   A     �  
   �     �  K   �     D  	   a  $   k  T   �     �  #   �     "  �  9     �     �               $     >     A     M     l  %   s      �  
   �     �     �     �     �     �                    0     J     `     h     {     �     �     �     �      �     �     
  	             (  
   0     ;     J  
   d     o     x     �  	   �     �     �     �  U   �     5     H     Q     e  
   x     �     �     �     �  
   �     �  
   �     �     �     �     �                    =     I     d     k     ~     �     �     �     �  	   �  K   �     )     9     K     P     e     {     �     �  	   �     �     �  W   �  
   0     ;  	   D  �   N     �  	   �     �  Y   
  !   d  	   �  #   �  I   �     �          .           "           1   h   a      _   B                  Z   S   )   b                 W              C       5   \   %       e       E              -   4       =   /       D      ^              V   9   I   *   ;          $   G              i               .   ?               >   @      (   R      g      <          U   ,   Y       0   +   ]       Q   8                  	   [         '   X   M          #   A      3       7       H       L   
      P                        2           K   d   f   `   O   :       c      &   F          T   !   J   6   N           % Comments %1$s designed by %2$s. %d item %d items %s is activated &larr; Older Comments ,  1 Comment <cite class="fn">%s</cite> About us Alternate button background color Alternate button text color An %s project Background Background color Below Header Buttons Cart Categories:  Checkout Comment navigation Comments are closed. Conditions of sale Contact Continue reading %s Customise the look & feel of your web site buttons. Customise the look & feel of your web site footer. Description Edit Enhance %s FAQ Featured Products Footer Footer %d Footer Menu Free! Full width General Layout Get started with Storefront Handheld Menu Header Header background image Heading color Home Homepage Install %s Installation &amp; configuration It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Latest %s news Layout Leave a comment Link / accent color Link color Log In Logo Looking for more options? More My Account My Social Profile Navigation New In Store Newer Comments &rarr; Next postNext Nothing Found On Sale On Sale Now Oops! That page can&rsquo;t be found. Optional Text Order Online or Call Pages: Popular Products Price calculator Primary Menu Primary Navigation Privacy policy Product Categories Products Ready to publish your first post? <a href="%1$s">Get started here</a>. Recent News Recent Products Search Search Products Search Results for: %s Secondary Menu Secondary Navigation Shop Sidebar Skip to content Skip to navigation Sorry, but nothing matched your search terms. Please try again with some different keywords. Storefront Tags:  Text color Thanks for choosing Storefront! You can read hints and tips on how get the most out of your new theme on the %swelcome screen%s. Top Rated Products Typography View your shopping cart Why not leave us a review on %sWordPress.org%s?  We'd really appreciate it! Widgetized Footer Region %d. WooThemes Your comment is awaiting moderation. comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; http://www.woothemes.com http://www.woothemes.com/storefront post datePosted on %s Project-Id-Version: Storefront
Report-Msgid-Bugs-To: https://github.com/woothemes/storefront/issues
POT-Creation-Date: 2016-06-16 17:23+0200
PO-Revision-Date: 2016-06-16 17:51+0200
Last-Translator: John Weissberg <jw@printscience.com>
Language-Team: 
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.8.6
 % kommentarer %1$s skapad av %2$s. %d vara %d varor %s är aktiverad &larr; Äldre kommentarer ,  1 kommentar <cite class=“fn”>%s</cite> Om oss Alternativ bakgrundsfärg på knappar Alternativ textfärg på knappar %s projekt Bakgrund Färg för bakgrund Under sidhuvud Knappar Kundvagn Kategorier:  Till kassan Kommentarsnavigering Kommentarer inaktiverade. Försäljningsvillkor Kontakt Fortsätt läsa %s Anpassa utseende på knappar Anpassa utseende på sidfoten Produktdata Redigera Förbättra %s FAQ (tidigare besvarade frågor) Heta produkter Sidfot Sidfot %d Sydfots meny Gratis! full bredd Allmän layout Kom igång med Storefront Mobil meny Sidhuvud Bakgrundsbild för sidhuvud Rubrikfärg Startsida Hemsida Installera %s Installation och konfiguration Det verkar som det du letade efter inte kunde hittas. Kanske kan en sökning hjälpa. Senaste %s nyheter Utseende Lämna en kommentar Länk-/accentfärg Länkfärg Logga In Logo Fler alternativ? Mer Mitt konto Sociala profiler Navigation Nytt i shoppen Nyare kommentarer &rarr; Nästa Inget kunde hittas På REA REA nu Oops! Sidan kunde inte hittas. Valfri Text Beställ Online eller ring Sidor: Omtyckta produkter Pris kalkylatorn Primär meny Primär navigation Integritetspolicy Produktkategorier Produkter Är du redo att posta ditt första inlägg? <a href="%1$s">Starta här</a>. Senaste nyheter Senaste produkter Søk Sök bland produkter Sökresultat för: %s Sekundär meny Sekundär navigering Shopp Sidopanel Gå till innehåll Hoppa till navigering Ledsen, inget matchade dina sökkriterier. Vänligen försök igen med andra nyckelord. Storefront Taggar:  Textfärg Tack för att du valt Storefront! Du kan läsa tips och tricks om hur du får ut mest möjliga ur ditt nya tema på %välkomstskärmen%s. Topprankade produkter Typografi Visa din varukorg Varför inte lämna ett omdöme på %sWordpress.org%s? Vi skulle verkligen uppskatta det! Område i sidfot för widgets %d. WooThemes Din kommentar inväntar granskning. En reaktion på &rdquo;%2$s&rdquo; %1$s reaktioner på &rdquo;%2$s&rdquo; http://printscience.com http://printscience.com Inlägget gjort %s 