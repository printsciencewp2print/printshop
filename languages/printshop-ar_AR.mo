��    O      �  k         �  
   �     �     �     �       	          !   *     L  
   h     s     �     �     �     �     �     �     �  3   �  2        P     \     a     e     w  	   ~     �     �  
   �     �     �     �     �     �  
   �  W   	     Z	     a	  
   q	     |	     �	     �	  
   �	  
   �	     �	     �	     �	     �	     �	  %   �	     
     
     &
     3
     F
     U
  F   h
     �
     �
     �
     �
     �
     �
                 \   .  
   �  
   �  �   �     "  
   5     @     X  	   u  $     T   �     �  �              c   ;  &   �     �     �  !   �  "      !   #  
   E     P     f     u  
   �     �     �     �     �  ;   �  9   8     r  
   �  &   �     �  $   �     �          #     /  -   C     q     ~     �     �     �  e   �     ;     Q     c     w     �     �  
   �  
   �     �  &   �  &   �  $        0  A   G     �     �     �  (   �     �       o   0     �     �     �     �     �  (        E     R      n  �   �     V     c  �   s          (     <  :   X     �  -   �  �   �     �     !                        %   	   7       -      I      +       &   F   <      
   (           3       :            9   "   $   C       .   @             O       6         G   D                      M       B   N   *      '   E   5   ,   4       /   ?          1   )   ;   K           L           A      H          =                  #   2                        0   >         J      8             % Comments %1$s designed by %2$s. %d item %d items &larr; Older Comments ,  1 Comment <cite class="fn">%s</cite> Alternate button background color Alternate button text color Background Background color Buttons Cart Checkout Comment navigation Comments are closed. Conditions of sale Continue reading %s Customise the look & feel of your web site buttons. Customise the look & feel of your web site footer. Description Edit FAQ Featured Products Footer Footer %d Footer Menu Free! Full width Get started with Storefront Header Header background image Heading color Homepage Install %s It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Layout Leave a comment Link color Log In Logo More My Account Navigation New In Store Newer Comments &rarr; Nothing Found On Sale On Sale Now Oops! That page can&rsquo;t be found. Pages: Price calculator Primary Menu Primary Navigation Privacy policy Product Categories Ready to publish your first post? <a href="%1$s">Get started here</a>. Recent Products Search Search Products Search Results for: %s Secondary Menu Secondary Navigation Shop Sidebar Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Storefront Text color Thanks for choosing Storefront! You can read hints and tips on how get the most out of your new theme on the %swelcome screen%s. Top Rated Products Typography View your shopping cart Widgetized Footer Region %d. WooThemes Your comment is awaiting moderation. comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; post datePosted on %s Project-Id-Version: Storefront
Report-Msgid-Bugs-To: https://github.com/woothemes/storefront/issues
POT-Creation-Date: 2016-06-15 10:52+0200
PO-Revision-Date: 2016-06-15 10:56+0200
Last-Translator: John Weissberg <jw@printscience.com>
Language-Team: 
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
X-Generator: Poedit 1.8.6
 % تعليقات %1$s صمم بواسطة %2$s. لا يوجد منتجات %d منتج %d  منتج %d منتجات %d منتجات %d منتجات &rarr; التعليقات الأقدم ،  تعليق واحد ‏<cite class=“fn”>%s</cite> لون الخلفية البديل لون النص زر البديل خلفية لون الخلفية الازرار الطلبية الدفع تصفّح التعليقات التعليقات مغلقة. شروط البيع. متابعة قراءة %s تعديل شكل ومظهر الازرار في موقعك تعديل شكل ومظهر الفوتر في موقعك وصف المنتج تحرير الأسئلة الاكثر شيوعا منتجات مميزة أسفل الصفحة (الفوتر) الفوتر  %d. القائمة السفلى مجانا! كامل العرض إبدأ باستخدام قالب Storefront ترويسة صورة  رأس الخلفية لون العناوين الرئيسية تنصيب %s يبدو أنه لا يمكن إيجاد ما تبحث عنه. قد تفيدك خاصية البحث. الشكل العام أضف تعليق لون الرابط دخول الشعار المزيد حسابي ملاحة جديدنا التعليقات الأحدث &larr; لم يتم العثور على شيء منتجات باعلى خصومات صفقات مخفضة! عذراً! لايمكن العثور على تلك الصفحة. الصفحات: آلة حاسبة القائمة الأساسية عنوان القائمة الرئيسي سياسة الخصوصية تصنيفات المنتجات هل أنت على استعداد لنشر مقالتك الأولى؟ <a href="%1$s">ابدأ من هنا</a>. أحدث المنتجات بحث  البحث عن منتجات نتائج البحث عن: %s القائمة الثانية روابط التصفح الثانوية المتجر الشريط الجانبي انتقل إلى المحتوى عذرا، لا يوجد شيء يتطابق مع كلمات البحث التي استعملتها، المرجو المحاولة من جديد باستعمال كلمات مفتاحية أخرى. ‏Printshop لون النص شكرا لك على استخدام storefront . للحصول على أفضل نتائج من القالب اقرأ الملحوظات والخطوات  الأعلى تقييماً شكل الخطوط عرض سلة التسوق  منطقة الفوتر الخاصة بالودجات  %d. ‏Print Science تعليقك بانتظار المراجعة. لا توجد أراء حول &ldquo;%2$s&rdquo; رأي واحد حول &ldquo;%2$s&rdquo; رأيان حول &ldquo;%2$s&rdquo; %1$s رأي حول &ldquo;%2$s&rdquo; %1$s رأي حول &ldquo;%2$s&rdquo; %1$s رأي حول &ldquo;%2$s&rdquo; نشر في %s 