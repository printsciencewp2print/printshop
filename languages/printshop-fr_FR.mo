��    p      �  �         p	  
   q	     |	     �	     �	     �	     �	  	   �	     �	     �	  !   �	     
     9
  
   G
     R
     c
     p
     x
     }
     �
     �
     �
     �
     �
     �
     �
  3   �
  2   +     ^     j  
   o     z     �     �     �  	   �     �     �  
   �     �     �     �               #     1     6  
   ?     J      _  W   �     �     �     �     �  
             $     )  E   C     �  
   �     �  
   �     �     �     �     �     �     �  %   
     0     >     S     Z     k     �     �     �     �     �     �  F   �     %     1     A     H     X     o     ~     �     �     �     �  \   �  
         +     =  
   D  �   O     �  
   �     �       K   $     p  	   �  $   �  T   �          *     <  �  S     �               4      C     d     g     v     �  '   �  0   �                     9     R     Z     a     n     �     �     �     �     �     �  8     <   ?     |     �     �     �     �     �     �     �     �  	   �               .     H     T     ]     v     �     �     �  "   �     �  k   �     c     {     �     �     �  
   �     �  (   �  S        Y     ^     j  
   x     �  !   �     �     �     �     �  '   �          %     B     O     c     o     �     �     �     �  	   �  S   �     ;     O  	   a     k     �     �     �     �     �     �     �  d     	   k     u     �     �  �   �     i     �      �     �  l   �  '   3     [  /   i  u   �          '     >           $           4       h      e   F                 _   X   +   i                 \          p   G       8   b   '       l       I      `   B   /   7       A   2       H      d          ?       =   N   ,   <          &   K              L   "           0   C               1   D      *   W      n      @      o   Z   .   ^       3   -   c       V   ;                  	   [         )   ]   R          %   E      6       :       M       Q   
   !   U         a              5           P   k   m   f   T   >   g   j      (   J          Y   #   O   9   S           % Comments %1$s designed by %2$s. %d item %d items %s is activated &larr; Older Comments ,  1 Comment <cite class="fn">%s</cite> About us Alternate button background color Alternate button text color An %s project Background Background color Below Header Buttons Cart Categories:  Checkout Child themes Comment navigation Comments are closed. Conditions of sale Contact Continue reading %s Customise the look & feel of your web site buttons. Customise the look & feel of your web site footer. Description Edit Enhance %s Enjoying %s? FAQ Featured Products Footer Footer %d Footer Menu Free! Full width General Layout Get started with Storefront Handheld Menu Header Header background image Heading color Home Homepage Install %s Install free plugins Installation &amp; configuration It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Latest %s news Layout Leave a comment Link / accent color Link color Log In Logo Looking for more options? Looking to add a logo? Install the %sJetpack%s plugin! %sRead more%s. More My Account My Social Profile Navigation New In Store Newer Comments &rarr; Next postNext Nothing Found On Sale On Sale Now Oops! That page can&rsquo;t be found. Optional Text Order Online or Call Pages: Popular Products Previous postPrevious Price calculator Primary Menu Primary Navigation Privacy policy Product Categories Products Ready to publish your first post? <a href="%1$s">Get started here</a>. Recent News Recent Products Search Search Products Search Results for: %s Secondary Menu Secondary Navigation Shop Sidebar Skip to content Skip to navigation Sorry, but nothing matched your search terms. Please try again with some different keywords. Storefront Suggest a feature Tags:  Text color Thanks for choosing Storefront! You can read hints and tips on how get the most out of your new theme on the %swelcome screen%s. Top Rated Products Typography View all %s extensions &rarr; View your shopping cart Why not leave us a review on %sWordPress.org%s?  We'd really appreciate it! Widgetized Footer Region %d. WooThemes Your comment is awaiting moderation. comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; http://www.woothemes.com post authorby %s post datePosted on %s Project-Id-Version: Storefront
Report-Msgid-Bugs-To: https://github.com/woothemes/storefront/issues
POT-Creation-Date: 2016-06-15 11:00+0200
PO-Revision-Date: 2016-06-15 11:10+0200
Last-Translator: John Weissberg <jw@printscience.com>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Poedit 1.8.6
 % commentaires %1$s design par %2$s. %d article %d articles %s est activé &larr; Commentaires plus anciens ,  Un commentaire <cite class=“fn”>%s</cite> À propos de nous Couleur alternative du fond des boutons Couleur alternative de la texte dans les boutons Un projet %s Arrière-plan Couleur d&rsquo;arrière-plan En-dessous de l'en-tête Boutons Panier Catégories: Valider votre panier Thèmes enfants Navigation des commentaires Les commentaires sont fermés. Conditions de vente Contacte Continuer la lecture de %s Personnalisez l'apparence des boutons de votre site web. Personnalisez l'apparence du pied de page de votre site web. Description Modifier Améliorer %s Bénéficiant %s? FAQ Produits mis en avant Pied de page Pied de page %d Menu de pied-de-page Gratuit ! Pleine largeur Mise en page générale Démarrez avec Storefront Menu manuel En-tête Image de fond d'en-tête Couleur Ent&ecirc;te&nbsp;: Accueil Page d'accueil %s installé Installer les extensions gratuites Installation et configuration Il semblerait que nous ne soyons pas en mesure de trouver votre contenu. Essayez en lançant une recherche. Dernières %s nouvelles Mise en page Laisser un commentaire Lien / couleur décorative Couleur des liens Connection Logo À la recherche de plus d'options&nbsp;? Vous cherchez à ajouter un logo? Installez le %s Jetpack% plugin%! %s Lit plus %s. Plus Mon Compte  Profil social Navigation Nouveau en magasin Commentaires plus récents &rarr; Suivant Rien de trouvé En promo En vente maintenant Oups&nbsp;! Cette page est introuvable. Texte optionnel Commande en ligne ou appelez Pages&nbsp;: Produits populaires Précédent Calculateur de prix Menu principal Navigation principale Politique de confidentialité Catégories de produit Catalogue Prêt à publier votre premier article&nbsp;? <a href="%1$s">Lancez-vous</a>&nbsp;! Nouvelles récentes Produits récents Recherche Recherche de produits Résultats de recherche : %s  Menu secondaire Navigation secondaire Boutique Barre latérale Aller au contenu Aller à la navigation Désolé, mais rien ne correspond à votre recherche. Veuillez réessayer avec des mots différents. Printshop Suggérer une fonctionnalité Mots-clés: Couleur du texte Merci d&rsquo;avoir choisi Storefront&nbsp;! Vous pouvez lire des trucs et astuces sur la façon de tirer le meilleur parti de votre nouveau thème sur sl&rsquo;%écran de bienvenue%s. Produits les mieux notés Typographie Voir toutes %s extensions &rarr; Afficher votre panier Pourquoi ne pas nous laisser un commentaire sur %sWordPress.org%s&nbsp;? Nous apprécierions vraiment&nbsp;! Région %d de pied de page widgetizée. Print Science Votre commentaire est en attente de modération Une réflexion au sujet de &laquo;&nbsp;%2$s&nbsp;&raquo; %1$s réflexions au sujet de &laquo;&nbsp;%2$s&nbsp;&raquo; http://printscience.com par %s [ post author ] Publié le %s 