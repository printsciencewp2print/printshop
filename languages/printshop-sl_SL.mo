��    L      |  e   �      p  
   q     |     �  	   �     �  !   �     �  
   �     �                         %     8     M     `     h     |     �     �     �     �  	   �     �     �     �     �     �     �            
           W   <     �     �     �  
   �     �     �     �  
   �     �     �     	     	     )	     1	  %   =	     c	     q	     �	     �	     �	     �	     �	     �	     �	     �	  F   �	     A
     Q
     X
     h
     
     �
     �
     �
  \   �
  
          
   &  $   1  T   V  �  �     �     �     �  
   �     �     �     �                     !  	   '     1     7     Q     f  	   u          �     �     �     �     �     �  	   �     �     �     �     �     �     
       
   &     1  W   M  
   �  
   �     �     �     �     �     �  
   �     �     
          5     A     O      _     �  "   �     �     �     �     �     �     �          *  W   3     �     �     �     �     �     �     �     �  U        b     q     �  !   �  �   �     J       E      &   :   )   '                    2   .                      0          L                   /      9   >       $   +   3         4   1   G       (          D       "          	         7          F   A   #      I         H   B   6               5       
   <   @   =      8   -   ?   ;                        !       C   *      %              K       ,       % Comments &larr; Older Comments ,  1 Comment About us Alternate button background color Alternate button text color Background Background color Below Header Buttons Cart Checkout Comment navigation Comments are closed. Conditions of sale Contact Continue reading %s Description Edit FAQ Featured Products Footer Footer %d Footer Menu Free! General Layout Header Header background image Heading color Home Homepage Install %s Installation &amp; configuration It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Layout Leave a comment Link / accent color Link color Log In Logo More My Account My Social Profile New In Store Newer Comments &rarr; Nothing Found On Sale On Sale Now Oops! That page can&rsquo;t be found. Optional Text Order Online or Call Pages: Popular Products Price calculator Primary Menu Primary Navigation Privacy policy Product Categories Products Ready to publish your first post? <a href="%1$s">Get started here</a>. Recent Products Search Search Products Search Results for: %s Secondary Menu Shop Sidebar Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Text color Top Rated Products Typography Your comment is awaiting moderation. comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; Project-Id-Version: Storefront
Report-Msgid-Bugs-To: https://github.com/woothemes/storefront/issues
POT-Creation-Date: 2016-06-20 15:56+0200
PO-Revision-Date: 2016-06-20 16:00+0200
Last-Translator: John Weissberg <jw@printscience.com>
Language-Team: 
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Generator: Poedit 1.8.6
 % komentarjev &larr; Starejši komentarji , 1 komentar O nas Nadomestni gumb barva ozadja Nadomestni gumb barva besedila Ozadje Barva Ozadja spodaj glavi Gumbi Košarica Nakup Krmarjenje po komentarjih Komentiranje zaprto. Pogoji prodaje Kontaktne Preberi več o %s Opis Uredi FAQ Izbrani izdelki Noga Noga %d Meni Noga Brezplačno! splošno Postavitev Glava Glava ozadje Postavka barva Domača stran Domača stran Namesti %s Namestitev in konfiguracija Očitno ni bilo mogoče najti tega, kar iščete. Morda si lahko pomagate z iskalnikom. Postavitev Komentiraj Barva akcentov Barva povezave Prijava Logo Več Moj račun Moj socialni profil Novo v trgovini Novejši komentarji &rarr; Ni Zadetkov Na razprodaji Na prodajo zdaj Ups! Da strani ni mogoče najti. Izbirno besedilo Naročila na spletu ali pokličite Strani: Priljubljeni izdelki Kalkulator cene Glavni meni Glavna Navigacija Pravilnik o zasebnosti Kategorije izdelkov Izdelkov Ste pripravljeni na objavo vašega prvega prispevka? <a href="%1$s">Začnite tukaj</a>. Nedavni izdelki Išči Išči izdelke Rezultati iskanja za: %s Vedľajšej ponuke Trgovina Stranska vrstica Preskoči na vsebino Oprostite, z vašemu zahtevku se nič ne ujema. Prosim, poskusite z drugimi besedami. Barva besedila Najbolj ocenjeno izdelki Tipografija Vaš komentar čaka na odobritev. %1$s odziv na &ldquo;%2$s&rdquo; %1$s odziva na &ldquo;%2$s&rdquo; %1$s odzivi na &ldquo;%2$s&rdquo; %1$s odzivov na &ldquo;%2$s&rdquo; 