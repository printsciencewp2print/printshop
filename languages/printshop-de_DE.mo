��    q      �  �   ,      �	  
   �	     �	     �	     �	     �	     �	  	   �	     �	     

  !   
     5
  �   Q
     �
  
   �
     �
                         )     2     ?     R     g     z     �  3   �  2   �     �            
   "     -     1     ?     Q  	   X     b  �   n     '  
   -     8     G     U     \     t     �     �  
   �     �      �  W   �     )     8     ?     O  
   c     n     u  E   z     �  
   �     �  
   �     �     �            P   -     ~     �  %   �     �     �     �     �     �     
          (     ;     J     ]  F   f     �     �     �     �     �     �                     (     8  \   K  
   �     �     �  
   �  �   �  t   X     �  
   �     �       	      $   *     O  #   h     �     �  �  �     b     o     �     �     �     �     �     �  	   �  #   �     #  �   @     �     �               *  	   9     C  	   O     Y     e     z     �     �     �  $   �  $   �          0  
   =     H     Y     ]     t  	   �     �  
   �  �   �  
   �     �     �  
   �     �     �     �  
   �  
   �     
           ?  W   `     �     �      �     �  
     	          I        g  
   l     w  
   �     �     �     �     �  a   �  
   :     E  '   U     }  )   �     �     �     �     �  
   �     �          $     6  b   ?     �     �     �     �     �     �                       %      7   V   M   	   �      �      �   
   �   6   �   p   !     �!  
   �!     �!     �!     �!  '   �!     "     #"     ;"     B"     .       `       f       q   F               9                        O      d             ^   c   i       P   e   J   X   D   1       *      ;   S   ]      H   N       B   a   /              >   =   ,   V   6   M           %   <   $   R       )       4       K   &   0   3   o   (              W       T       m       _                  	      5   G   Y          Z   k       7            "   l   p       A   E   :          C   [   h      j               \   -      '   #                 I   L   U   g            ?   Q       @   n      !   +       b   8      
   2          % Comments %1$s designed by %2$s. %d item %d items %s is activated &larr; Older Comments ,  1 Comment <cite class="fn">%s</cite> About us Alternate button background color Alternate button text color Although %s works fine as a regular WordPress theme, it really shines when used for an online store. Install %s and start selling now. An %s project Background Background color Below Header Buttons Cart Categories:  Checkout Child themes Comment navigation Comments are closed. Conditions of sale Contact Continue reading %s Customise the look & feel of your web site buttons. Customise the look & feel of your web site footer. Customizer settings Description Edit Enhance %s FAQ Featured Docs Featured Products Footer Footer %d Footer Menu Found a bug? Want to contribute a patch or create a new feature? %sGitHub is the place to go!%s Or would you like to translate %s into your language? %sGet involved on WordPress.org%s. Free! Full width General Layout Handheld Menu Header Header background image Heading color Home Homepage Install %s Install free plugins Installation &amp; configuration It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Latest %s news Layout Leave a comment Link / accent color Link color Log In Logo Looking to add a logo? Install the %sJetpack%s plugin! %sRead more%s. More My Account My Social Profile Navigation New In Store Newer Comments &rarr; Next postNext Nothing Found Nothing was found at this location. Try searching, or check out the links below. On Sale On Sale Now Oops! That page can&rsquo;t be found. Optional Text Order Online or Call Pages: Popular Products Previous postPrevious Price calculator Primary Menu Primary Navigation Privacy policy Product Categories Products Ready to publish your first post? <a href="%1$s">Get started here</a>. Recent News Recent Products Search Search Products Search Results for: %s Secondary Menu Secondary Navigation Shop Sidebar Skip to content Skip to navigation Sorry, but nothing matched your search terms. Please try again with some different keywords. Storefront Suggest a feature Tags:  Text color Thanks for choosing Storefront! You can read hints and tips on how get the most out of your new theme on the %swelcome screen%s. There are a number of free plugins available for %s on the WordPress.org %splugin repository%s. Here are just a few: Top Rated Products Typography View your shopping cart Widgetized Footer Region %d. WooThemes Your comment is awaiting moderation. http://www.woothemes.com http://www.woothemes.com/storefront post authorby %s post datePosted on %s Project-Id-Version: Storefront
Report-Msgid-Bugs-To: https://github.com/woothemes/storefront/issues
POT-Creation-Date: 2016-06-12 11:07+0200
PO-Revision-Date: 2016-06-18 15:05+0200
Last-Translator: John Weissberg <jw@printscience.com>
Language-Team: LANGUAGE <info@printscience.com>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 1.8.6
X-Poedit-Basepath: .
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Loco-Target-Locale: de_DE
X-Poedit-SearchPath-0: ../../themes/printshop
 % Kommentare %1$s entwickelt von %2$s. %d Artikel %d Artikelen %s aktiviert &larr; ältere Kommentare , 1 Kommentar <cite class=“fn”>%s</cite> Über uns Alternative Button Hintergrundfarbe Alternative Button Textfarbe Obwohl%s als regelmäßige Wordpress-Theme funktioniert gut, scheint es wirklich, wenn für einen Online-Shop verwendet. Installieren%s und starten Sie jetzt verkaufen. Ein Projekt %s Hintergrund Hintergrundfarbe: Unter der Kopfzeile Schaltflächen Warenkorb Kategorien: Zur Kassa Kind-Themen Kommentar Navigation Kommentare sind geschlossen. Allgemeine Verkaufsbedingungen Kontakt %s weiterlesen Passen Sie die Optik der Buttons an. Passen Sie die Optik des Footers an. Customizer Einstellungen Beschreibung Bearbeiten %s zu verbessern FAQ Ausgewählte Dokumente Empfohlene Produkte Fußzeile Fußzeile %d Footermenu Gefunden einen Bug? Wollen Sie dazu beitragen einen Patch oder erstellen Sie ein neues Feature? %sGitHub ist der Ort zu gehen! %s oder möchten Sie %s in deine Sprache übersetzen? %sGet am WordPress.org%s beteiligt. Kostenlos! Ganze Breite allgemeines Layout Mobilmenü Überschriften Header Hintergrundbild Überschriften Farbe Startseite Startseite Installiere %s jetzt Kostenlose Plugins installieren Installation &amp; Konfiguration Es scheint, dass wir nicht finden, wonach di suchst. Vielleicht hilft dir das Suchfeld. %s-Neuigkeiten Layout Hinterlassen Sie Ihren Kommentar Link- / Akzentfarbe Link-Farbe Einloggen Logo Logo hinzufügen? Installieren Sie %sJetpack%s Plugin! %sMehr erfahren%s. Mehr Mein Konto Meine Social Media-Profil Navigation Neu im Shop neuere Kommentare &rarr; Weiter Nichts gefunden Keine Einträge gefunden. Bitte nutzen Sie die Suche oder besuchen Sie die unten stehenden Links. Reduziert! Jetzt reduziert Diese Seite kann nicht gefunden werden. Optionaler Text Bestellen Sie online oder rufen Sie - an. Seiten: Beliebte Artikel Zurück Preiskonfigurator Hauptmenü Haupt-Navigation [links] Datenschutzerklärung Produktkategorien Produkte Sind Sie bereit, Ihren ersten Beitrag zu veröffentlichen? <a href=“%s”>Beginnen Sie hier</a>. Neueste Beiträge Neuste Produkte Suche Produkte suchen Suchergebnisse für: %s Sekundäres Menü Sekundärnavigation Shop Sidebar Weiter zum Inhalt Weiter zur Navigation Die Suche ergab leider keine Treffer. Bitte versuche es mit einem anderen Suchbegriff. PrintShop Eine Funktion vorschlagen Schlagworte: Textfarbe: Danke, dass Sie sich für Printshop entschieden haben. Für %s auf WordPress.org %splugin Repository %s gibt es eine Reihe von kostenlosen Plugins. Hier sind ein paar: Bestbewertete Produkte Typography Warenkorb ansehen Fußzeile mit Widgets %d Print Science Ihr Kommentar wartet auf Freischaltung. http://printscience.com http://printscience.com von %s Geschrieben am %s 