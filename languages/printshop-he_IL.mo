��    F      L  a   |         
             "  	   %     /  
   8     C     T     \     a     j     }     �     �     �     �     �     �     �     �     �  
   �                       
   )  W   4     �     �     �  
   �     �     �  
   �     �     �     �               $  %   0     V     d     y     �     �     �     �     �     �     �  F   �     4	     D	     K	     [	     r	     �	     �	     �	  \   �	  
   �	     
  
   
     $
  $   <
  T   a
     �
  �  �
     s  '   �     �     �     �     �     �     �     �     �                9     V     d       
   �     �     �     �     �     �     �     �     
          !  n   6  
   �     �     �     �  
   �     �               .  '   I     q     �     �  &   �     �  %   �       #   %  
   I     T     h     |     �     �  \   �          6     =     U     q     �     �     �  �   �     7  +   I     u     �  '   �  S   �             ?   E       1             	   4                 9      6       
   0   C   5           #                                  ,       @       2   F      .   )              %         D          '   "   (   A             /       :       B       $          >             !   <         3   +   7                     -       =       8   ;   *             &       % Comments &larr; Older Comments ,  1 Comment About us Background Background color Buttons Cart Checkout Comment navigation Comments are closed. Conditions of sale Contact Continue reading %s Description Edit FAQ Featured Products Footer Footer Menu Full width Header Heading color Home Homepage Install %s It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Layout Leave a comment Link / accent color Link color Log In More My Account My Social Profile New In Store Newer Comments &rarr; Nothing Found On Sale On Sale Now Oops! That page can&rsquo;t be found. Optional Text Order Online or Call Pages: Popular Products Price calculator Primary Menu Primary Navigation Privacy policy Product Categories Products Ready to publish your first post? <a href="%1$s">Get started here</a>. Recent Products Search Search Products Search Results for: %s Secondary Menu Shop Sidebar Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Text color Top Rated Products Typography View your shopping cart Your comment is awaiting moderation. comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; post datePosted on %s Project-Id-Version: Storefront
Report-Msgid-Bugs-To: https://github.com/woothemes/storefront/issues
POT-Creation-Date: 2016-06-18 15:11+0200
PO-Revision-Date: 2016-06-22 17:03+0200
Last-Translator: John Weissberg <jw@printscience.com>
Language-Team: 
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.8.6
 % תגובות &rarr; תגובות ישנות יותר ,  תגובה אחת אודותינו רקע צבע רקע כפתורים סל להזמנה ניווט בתגובות סגור לתגובות. תנאים של התצוגה איש קשר להמשיך לקרוא %s תאור עריכה FAQ מוצרים נבחרים פוטר תפריט תחתון רוחב מלא תמונת כותרת צבע הכותרת ראשי דף הבית להתקין את %s נראה שלא הצלחנו למצוא את מה שחיפשת. ייתכן שביצוע חיפוש יסייע. פריסה כתיבת תגובה צבע הדגשה צבע הקישורים כניסה עוד הספריה שלי פרופיל חברתי הוסף חנות חדשה תגובות חדשות יותר &larr; לא מצאתי כלום במכירה עכשיו במבצע! העמוד המבוקש לא נמצא. טקסט אופציונלי שיחה או הזמנה מקוונת :עמודים המוצרים הפופולריים חישוב תפריט ראשי ניווט ראשי מדיניות פרטיות קטגורית מוצר מוצרים מוכן לפרסם את הפוסט הראשון שלך? <a href="%1$s">התחל כאן</a>. מוצרים אחרונים חפש חפש מוצרים … תוצאות חיפוש: %s תפריט משני חנות  סרגל צדדי לדלג לתוכן לא נמצאו תוצאות התואמות לביטוי החיפוש, ניתן לנסות שוב עם ביטויים אחרים. צבע הטקסט המוצרים המומלצים  ביותר טיפוגרפיה צפה בסל הקניות תגובתך ממתינה לאישור. מחשבה אחת על &ldquo;%2$s&rdquo; %1$s מחשבות על &ldquo;%2$s&rdquo; פורסם בתאריך %s 