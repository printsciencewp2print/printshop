��    D      <  a   \      �  
   �     �     �          #  	   &     0     K  
   Y     d     u     �     �     �     �     �     �     �  
   �     �     �            
        $     +  
   4      ?  W   `     �     �  
   �     �     �  
   �     �     �          "     *  %   6     \     c     t     �     �     �     �  F   �     	     	     %	     5	     L	     [	     c	  \   s	  
   �	  
   �	     �	  
   �	     
  $   
  T   A
     �
  #   �
     �
  �  �
  
   �  	   �     �     �     �     �     �     �     �                    %     2     E     R     a     i     p     y     �     �     �     �     �     �     �     �  Q   �     /     6     C     P     W     ^  	   k     u  	   �     �  	   �  '   �  	   �     �  	   �  	   �     �     
       I   $     n     ~     �     �     �     �     �  N   �  	             +     A     H     ^     }     �     �     �        :          .                 1                  5       2          -   ?   A           #                                          <       0   C         (   B          /   "       @         &   8   '                ,   	   6       >      9          =             !       $         *   3   ;              
   D       +       4   7   )              %       % Comments %d item %d items %s is activated &larr; Older Comments ,  1 Comment <cite class="fn">%s</cite> An %s project Background Background color Below Header Checkout Comment navigation Comments are closed. Conditions of sale Continue reading %s Description Edit Enhance %s FAQ Featured Products Footer Free! Full width Header Homepage Install %s Installation &amp; configuration It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Layout Leave a comment Link color Log In More My Account New In Store Newer Comments &rarr; Nothing Found On Sale On Sale Now Oops! That page can&rsquo;t be found. Pages: Popular Products Price calculator Primary Menu Primary Navigation Privacy policy Product Categories Ready to publish your first post? <a href="%1$s">Get started here</a>. Recent Products Search Search Products Search Results for: %s Secondary Menu Sidebar Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Storefront Text color Top Rated Products Typography View your shopping cart Your comment is awaiting moderation. comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; http://www.woothemes.com http://www.woothemes.com/storefront post datePosted on %s Project-Id-Version: Storefront
Report-Msgid-Bugs-To: https://github.com/woothemes/storefront/issues
POT-Creation-Date: 2016-06-15 16:56+0200
PO-Revision-Date: 2016-06-15 17:02+0200
Last-Translator: John Weissberg <jw@printscience.com>
Language-Team: 
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.8.6
 %条评论 %d 項目 %s 已激活 &larr; 早期评论 、 一条评论 <cite class=“fn”>%s</cite> %s项目 背景 背景颜色 在页头下 查看 评论导航 评论已关闭。 销售条款 继续阅读%s 描述  编辑 改善%s 常见问题回答(FAQ) 特色产品 底部 免费! 全宽 顶部 主页 安装%s 安装&amp;配置 我们可能无法找到您需要的内容。或许搜索功能可以帮到您。 布局 留下评论 链接颜色 登入 更多 我的帐户 新商店 较新评论 &rarr; 未找到 促销产品 已发售 有点尴尬诶！该页无法显示。 页面： 热门产品 计算器 主菜单 主导航栏 隐私政策 产品分类 准备好发布第一篇文章了？<a href="%1$s">从这里开始</a>。 最近的产品 搜索 产品搜索 %s的搜索结果 二级菜单 边栏 跳至正文 抱歉，没有符合您搜索条件的结果。请换其它关键词再试。 Printshop 文本颜色 最受欢迎的产品 排版 瀏覽購物車清單 您的评论正等待审核。 《%2$s》有%s个想法 http://printscience.com http://printscience.com 在 %s 上张贴 