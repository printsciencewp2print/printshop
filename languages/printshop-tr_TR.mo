��    @        Y         �  
   �     �     �     �     �  	   �  !   �     �  
             *     7     J     _     r     �     �     �  	   �     �     �  
   �     �     �     �     �  
   �      �  W        w     �     �     �  
   �     �     �  
   �     �  
   �     �       %        :     A     R     _     r     �  F   �     �     �     �     �     	     $	     9	     A	  \   Q	  
   �	  
   �	     �	  $   �	  T   
  �  V
     �               %     9     <  $   D      i     �     �     �     �     �     �     �     �               +     7  
   C  	   N     X  #   e     �  	   �  	   �     �  j   �     0  	   @     J     W     c  
   t  
        �     �  
   �     �     �     �  	   �     �               .     C  W   R     �     �     �     �     �     �  	          y        �  	   �  "   �  !   �  L   �           "                                   8   >       *   ,         #   (       )            $          0                 9   +              @   ;   :      7          
   !         %           <   5   -         '       /   2              =                    	              4   ?          1   &       3      .       6        % Comments %d item %d items %s is activated &larr; Older Comments ,  1 Comment Alternate button background color Alternate button text color Background Background color Child themes Comment navigation Comments are closed. Conditions of sale Continue reading %s Edit FAQ Footer Footer %d Footer Menu Free! Full width Header Header background image Heading color Homepage Install %s Installation &amp; configuration It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Latest %s news Layout Leave a comment Link / accent color Link color Log In More My Account My Social Profile Navigation Newer Comments &rarr; Nothing Found Oops! That page can&rsquo;t be found. Pages: Popular Products Primary Menu Primary Navigation Privacy policy Product Categories Ready to publish your first post? <a href="%1$s">Get started here</a>. Recent News Search Search Products Search Results for: %s Secondary Menu Secondary Navigation Sidebar Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Text color Typography View your shopping cart Your comment is awaiting moderation. comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; Project-Id-Version: Storefront
Report-Msgid-Bugs-To: https://github.com/woothemes/storefront/issues
POT-Creation-Date: 2016-06-15 17:03+0200
PO-Revision-Date: 2016-06-15 17:08+0200
Last-Translator: John Weissberg <jw@printscience.com>
Language-Team: 
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.6
 % Yorum %d ürün %d ürünler %s etkin &arr; Eski yorumlar ,  1 Yorum Alternatif düğmesi arka plan rengi Alternatif düğmesi metin rengi Arkaplan Arka plan rengi Alt tema Yorum dolaşımı Yorumlar kapalı. Satış koşulları Okumaya devam et %s Düzenle Sıkça Sorulan Sorular (SSS) Alt Kısım Altbilgi %d Taban Menü Ücretsiz! Tam Sayfa Üst Kısım Üst Bilgi (Header) Arka Plan Resmi Heading rengi Ana Sayfa Yükle %s Kurulum ve yapılandırma Aradığınız sayfa bulunamadı. Belki arama kutusu aradığınızı bulma konusunda yardımcı olabilir. Son %s Haberler Yerleşim Yorum yapın Vurgu Rengi Bağlantı rengi Giriş Yap Daha fazla Üye hesabım Sosyal Profiller Navigasyon Yeni yorumlar &arr; Bulunamadı Off! Bu sayfa bulunamadı. Sayfalar: Popüler Ürünler Birincil menü Birincil dolaşım Gizlilik Politikası Ürü Kategori İlk yazınızı yayınlamaya hazır mısınız? <a href="%1$s">Buradan başlayın</a>. Son Haberler Arama Ürün Arama Arama sonuçları: %s İkincil menü İkincil Navigasyon Yan Menü İçeriğe atla Üzgünüz fakat aradığınız kriterler ile örtüşen sonuç bulunamadı. Lütfen başka kriterler ile tekrar deneyin. Metin rengi Tipografi Alışveriş sepetini görüntüle Yorumunuz denetim için bekliyor. &ldquo;%2$s&rdquo; üzerine bir yorum &ldquo;%2$s&rdquo; üzerine %1$s yorum 