<?
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * storefront engine room
 *
 * @package storefront
 */

define( 'AIWOO_VERSION', '1.3' );
define('AI_WOO_PATH', plugin_dir_path( __FILE__ ));

/**
 * Initialize all the things.
 */
require get_template_directory() . '/inc/init.php';


/**

 * Check if WooCommerce is active

 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	// GET ALL CATEGORIES
	function wccp_set_categories_prices() {
		$taxonomy     = 'product_cat';
		$orderby      = 'name';  
		$show_count   = 0;
		$pad_counts   = 0; 
		$hierarchical = 1;
		$title        = '';  
		$empty        = 0;
		$args = array(
		  'taxonomy'     => $taxonomy,
		  'orderby'      => $orderby,
		  'show_count'   => $show_count,
		  'pad_counts'   => $pad_counts,
		  'hierarchical' => $hierarchical,
		  'title_li'     => $title,
		  'hide_empty'   => $empty
		);
		$all_categories = get_categories( $args );

		if  ( $all_categories )  { 
			foreach ( $all_categories as $category ) { 
	//			print '<h1>CAT : </h1>' . $category->name;
				$category_id = $category->term_id;
				$all_products = get_posts(array(
					'post_type' => 'product',
					'tax_query' => array(
						array(
						'taxonomy' => 'product_cat',
						'field' => 'term_id',
						'terms' => $category_id)
					)
				));

				$prices = array(); // Prepare the Prices array
				foreach ( $all_products as $product ) { 
					$sale = get_post_meta( $product->ID, '_sale_price', true);
					$price = get_post_meta( $product->ID, '_regular_price', true);
					if ( $sale ) {
						$prices[] = $sale;	
					} elseif ( $price ) {
						$prices[] = $price;
					}
				} // END FOREACH $products

				$min = 0;
				$max = 0;

				if ( $prices ) {
	//				print '<h1>PRICES : </h1>' . print_r($prices);
					$min = min(array_values($prices));
	//				print '<h1>PRICES for '. $category_id .': </h1>' . $min . ' - ' . ;
				}

				update_term_meta ( $category_id, 'min_price', $min); 

			} // END FOREACH $all_categories

		} // END IF PRICES

	}

	add_action('init', 'wccp_set_categories_prices');

	

	

	// OUTPUT THE CATEGORY PRICE 

	function wccp_display_categories_prices($category) {

		$before = '<span class="price">From: ';

		$after = '</span>';

		$separator 	= get_option('ola_wccp_separator', '-'); 

		$currency 	= get_woocommerce_currency_symbol();

		// @TODO reuse the Price Format set by WooCommerce

		// $format 	= get_woocommerce_price_format();

		

		$category_id = $category->term_id;

	

		$min = get_woocommerce_term_meta ( $category_id, 'min_price' , true);

		

		if ( $min == $max  && $min > 0 ) {

			// @TODO

			// echo $before . $format . $after;

			echo $before . $currency . $min  . $after;

			$schema = 1;

		} elseif ( $min  ) {

			// @TODO

			// echo $before . $formatmin. $separator . $formatmax . $after;		

			echo $before . $currency . $min  ;	

			$schema = 2;

		} else { 

			echo $before . __('Free', 'olalaweb') . $after;

			$schema = 0;

		}

	

		if ( $schema = 1 ) {	echo

		'<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

			<meta itemprop="price" content="'.$min.'" />

			<meta itemprop="priceCurrency" content="'.$currency.'" />

		</div>

		';

		} elseif ($schema = 2) {	echo

		'<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

			<div itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer" style="display:none;">

				<div itemprop="lowPrice">'.$min.'</div>

				

				

			</div>    

			<meta itemprop="priceCurrency" content="'.$currency.'" />

		</div>

		';		

		}

	}

	

	// DISPLAY THE PRICE AFTER CATEGORY LOADED
	$woocommerce_disable_product_list_price = get_option('woocommerce_disable_product_list_price');
	if ($woocommerce_disable_product_list_price != 'yes') {
		add_action( 'woocommerce_after_subcategory', 'wccp_display_categories_prices' );
	}

	add_action( 'woocommerce_single_product_summary', 'wccp_single_product_summary', 9 );
	function wccp_single_product_summary() {
		echo '<h2 class="images-title">'.__('Description', 'printshop').'</h2>';
		echo '<h2 class="summary-title">'.__('Price calculator', 'printshop').'</h2>';
	}

	//add_filter( 'body_class', 'wccp_body_class' );
	function wccp_body_class($classes) {
		global $post;
		if (is_single() && $post->post_type == 'product') {
			$classes[] = 'storefront-full-width-content';
		}
		return $classes;
	}

	add_filter( 'woocommerce_product_settings', 'wccp_product_settings', 10, 2 );
	function wccp_product_settings($settings) {
		$field_added = false;
		$new_settings = array();
		foreach($settings as $k => $v) {
			if ($v['type'] == 'sectionend' && !$field_added) {
				$new_settings[] = array(
					'title'    => __( 'Products Per Page', 'printshop' ),
					'desc'     => __( 'Use -1 value for unlimited.', 'printshop' ),
					'id'       => 'woocommerce_products_per_page',
					'type'     => 'number',
					'default'  => '12',
					'css'      => 'width:50px;',
				);
				$field_added = true;
			}
			$new_settings[] = $v;
		}
		return $new_settings;
	}

	add_filter( 'loop_shop_per_page', 'wccp_loop_shop_per_page', 10, 2 );
	function wccp_loop_shop_per_page($prod_per_page) {
		$woocommerce_products_per_page = get_option( 'woocommerce_products_per_page' );
		if (strlen($woocommerce_products_per_page)) {
			$prod_per_page = $woocommerce_products_per_page;
		}
		return $prod_per_page;
	}
}


/* Add a metabox in admin menu page */

add_action('admin_head-nav-menus.php', 'aiwoo_add_nav_menu_metabox');

function aiwoo_add_nav_menu_metabox() {

	add_meta_box( 'aiwoo', __( 'AI WooCommerce Links' ) . ' v' . AIWOO_VERSION, 'aiwoo_nav_menu_metabox', 'nav-menus', 'side', 'default' );

}



/* The metabox code */

function aiwoo_nav_menu_metabox( $object )

{

	global $nav_menu_selected_id;



	$elems = array( '#aiwooshop#' => __( 'Shop' ), '#aiwoocart#' => __( 'Cart' ), '#aiwoobasket#' => __( 'Basket' ), '#aiwoologin#' => __( 'Log In' ), '#aiwoologout#' => __( 'Log Out' ), '#aiwoologinout#' => __( 'Log In' ).'|'.__( 'My Account' ), '#aiwoocheckout#' => __( 'Checkout' ), '#aiwooterms#' => __( 'Terms' ), '#aiwoomyaccount#' => __( 'My Account' ), '#aiwoosearch#' => __( 'Search Product' ).'|'.__( 'Search' )  );

	class aiwoologItems {

		public $db_id = 0;

		public $object = 'aiwoolog';

		public $object_id;

		public $menu_item_parent = 0;

		public $type = 'custom';

		public $title;

		public $url;

		public $target = '';

		public $attr_title = '';

		public $classes = array();

		public $xfn = '';

	}



	$elems_obj = array();

	foreach ( $elems as $value => $title ) {

		$elems_obj[$title] = new aiwoologItems();

		$elems_obj[$title]->object_id	= esc_attr( $value );

		$elems_obj[$title]->title		= esc_attr( $title );

		$elems_obj[$title]->url			= esc_attr( $value );

	}



	$walker = new Walker_Nav_Menu_Checklist( array() );

	?>

	<div id="login-links" class="loginlinksdiv">



		<div id="tabs-panel-login-links-all" class="tabs-panel tabs-panel-view-all tabs-panel-active">

			<ul id="login-linkschecklist" class="list:login-links categorychecklist form-no-clear">

				<?php echo walk_nav_menu_tree( array_map( 'wp_setup_nav_menu_item', $elems_obj ), 0, (object)array( 'walker' => $walker ) ); ?>

			</ul>

		</div>



		<p class="button-controls">

			<span class="list-controls hide-if-no-js">

				<a href="javascript:void(0);" class="help" onclick="jQuery( '#help-login-links' ).toggle();"><?php _e( 'Help' ); ?></a>

				<span class="hide-if-js" id="help-login-links"><br /><a name="help-login-links"></a>

					<?php

						echo 'You can add a redirection page after the user\'s logout simply adding a relative link after the link\'s keyword, example <code>#aiwoologinout#index.php</code> or <code>#aiwoologout#index.php</code>.';	

					?>

				</span>

			</span>

			<span class="add-to-menu">

				<input type="submit"<?php disabled( $nav_menu_selected_id, 0 ); ?> class="button-secondary submit-add-to-menu right" value="<?php esc_attr_e('Add to Menu'); ?>" name="add-login-links-menu-item" id="submit-login-links" />

				<span class="spinner"></span>

			</span>

		</p>



	</div>

	<?php

}



/* Modify the "type_label" */

add_filter( 'wp_setup_nav_menu_item', 'aiwoo_nav_menu_type_label' );

function aiwoo_nav_menu_type_label( $menu_item )

{

	$elems = array( '#aiwooshop#', '#aiwoocart#', '#aiwoobasket#', '#aiwoologin#', '#aiwoologout#', '#aiwoologinout#', '#aiwoocheckout#', '#aiwooterms#', '#aiwoomyaccount#', '#aiwoosearch#' );

	

	$menu_item_array = explode('#', $menu_item->url);

	$menu_item_url = '';

	if(!empty($menu_item_array[1]))

	{	

		$menu_item_url = '#'.$menu_item_array[1].'#';

	}

		

	if ( isset($menu_item->object, $menu_item->url) && $menu_item->object == 'custom' && in_array($menu_item_url, $elems) )

		$menu_item->type_label = ( 'AI WooCommerce' );



	return $menu_item;
}
if( !defined( 'ABSPATH' ) )

	die( 'Error' );



/* Used to return the correct title for the double login/logout menu item */

function aiwoo_loginout_title( $title ) {

	$titles = explode( '|', $title );

	if ( ! is_user_logged_in() )

		return esc_html( isset( $titles[0] ) ? $titles[0] : $title );

	else

		return esc_html( isset( $titles[1] ) ? $titles[1] : $title );

}



/* Used to return the correct title for the double basket menu item */

function aiwoo_basket_title( $title ) {

	global $woocommerce;

	if ( WC()->cart->get_cart_contents_count() == 0 )

		return esc_html( isset( $title ) ? $title : $title );

	else

		return sprintf (_n( '%d item', '%d items', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ) .' - '. WC()->cart->get_cart_total();

}



/* The main code, this replace the #keyword# by the correct links with nonce ect */

add_filter( 'wp_setup_nav_menu_item', 'aiwoo_setup_nav_menu_item' );

function aiwoo_setup_nav_menu_item( $item ) {

	global $pagenow, $woocommerce;

	if( $pagenow!='nav-menus.php' && !defined('DOING_AJAX') && isset( $item->url ) && strstr( $item->url, '#aiwoo' ) != '' ) {

		$item_url = substr( $item->url, 0, strpos( $item->url, '#', 1 ) ) . '#';

		$item_redirect = str_replace( $item_url, '', $item->url );

		

		if(!empty($item_redirect)) {

			$item_redirect = $item_redirect;

		} else if (function_exists('woocommerce_get_page_id')) {

			$item_redirect = get_permalink( woocommerce_get_page_id( 'myaccount' ) );
		}
		
		if (function_exists('woocommerce_get_page_id')) {
			switch( $item_url ) {

				case '#aiwooshop#'		 :	$item->url = get_permalink( woocommerce_get_page_id( 'shop' ) ); break;

				

				case '#aiwoocart#'		 :	$item->url = get_permalink( woocommerce_get_page_id( 'cart' ) ); break;

				

				case '#aiwoobasket#'	 :	$item->url = get_permalink( woocommerce_get_page_id( 'cart' ) ); 

											$item->title = aiwoo_basket_title( $item->title ); break;

				

				case '#aiwoologin#'		 : 	$item->url = get_permalink( woocommerce_get_page_id( 'myaccount' ) ); break;

				

				case '#aiwoologout#'	 : 	$item->url = wp_logout_url( $item_redirect ); break;

				

				case '#aiwoologinout#'	 :	$item->url = is_user_logged_in() ? ( $item_redirect ) : get_permalink( woocommerce_get_page_id( 'myaccount' ) );

											$item->title = aiwoo_loginout_title( $item->title ) ; break;

				

				case '#aiwoocheckout#'	 :	$item->url = get_permalink( woocommerce_get_page_id( 'checkout' ) ); break;	

				

				case '#aiwooterms#'	 	 :	$item->url = get_permalink( woocommerce_get_page_id( 'terms' ) ); break;

				

				case '#aiwoomyaccount#'	 :	$item->url = get_permalink( woocommerce_get_page_id( 'myaccount' ) ); break;



				case '#aiwoosearch#'	 :	$titles = explode( '|', $item->title );

				$item->title = '<form action="'.esc_url( home_url( '/'  ) ).'" class="woocommerce-product-search" method="get" role="search">

					<input type="search" title="'.esc_attr_x( 'Search for:', 'label' ).'" name="s" value="'.get_search_query().'" placeholder="'.esc_html( isset( $titles[0] ) ? $titles[0] : $item->title ).'" class="search-field">

					<input type="submit" value="'.esc_html( isset( $titles[1] ) ? $titles[1] : esc_attr_x( 'Search', 'submit button' ) ).'">

					<input type="hidden" value="product" name="post_type">

				</form>'; break;

			}
		}

		$item->url = esc_url( $item->url );

	}

	return $item;

}


include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) { 



	add_action( 'plugins_loaded', function(){

			$filename  = "include/";

			$filename .= is_admin() ? "backend.inc.php" : "frontend.inc.php";

			if( file_exists( plugin_dir_path( __FILE__ ) . $filename ) )

				include( plugin_dir_path( __FILE__ ) . $filename );

	});

	

} else {

	add_action('admin_notices', 'aiwoo_plugin_admin_notices');

}



function aiwoo_plugin_admin_notices() {



	   $msg = sprintf( __( 'Please install or activate : %s.', $_SERVER['SERVER_NAME'] ), '<a href=https://wordpress.org/plugins/woocommerce style="color: #ffffff;text-decoration:none;font-style: italic;" target="_blank"/><strong>WooCommerce - excelling eCommerce</strong></a>' );

	   

	   echo '<div id="message" class="error" style="background-color: #DD3D36;"><p style="font-size: 16px;color: #ffffff">' . $msg . '</p></div>';   

	   

	   deactivate_plugins('woocommerce-menu-extension/woocommerce-menu-extension.php');

}
/// start top bar function


/**
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woothemes/theme-customisations
 */
 
$themename = "Color Scheme";
$shortname = "mst";
$options = array (
array( "name" => "Style Sheet",
	"desc" => "Enter the Style Sheet you would like to use for Sweet Ass Theme",
	"id" => $shortname."_style_sheet",
	"type" => "select",
	"options" => array("default", "green", "blue", "red"),
	"std" => "default"),
);
?>
<?php
function mytheme_add_admin() {
	global $themename, $shortname, $options;
	if ( isset($_GET['page']) && $_GET['page'] == basename(__FILE__) ) {
		if ( 'save' == $_REQUEST['action'] ) {
			foreach ($options as $value) {
				update_option( $value['id'], $_REQUEST[ $value['id'] ] );
			}
			foreach ($options as $value) {
				if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); }
			}
			header("Location: themes.php?page=functions.php&saved=true");
			die;
		} else if( 'reset' == $_REQUEST['action'] ) {
			foreach ($options as $value) {
				delete_option( $value['id'] ); }
				header("Location: themes.php?page=functions.php&reset=true");
				die;
			}
	}
	add_theme_page($themename." Options", "".$themename." Options", 'manage_options', basename(__FILE__), 'mytheme_admin');
}

function mytheme_admin() {
	global $themename, $shortname, $options;
	if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';
	if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';
	?>
	<div class="wrap">
	<h2><?php echo $themename; ?> Settings</h2>

	<form method="post">

	<?php foreach ($options as $value) {
	switch ( $value['type'] ) {
	case "open":
	?>
	<table width="100%" border="0" style="background-color:#eef5fb; padding:10px;">

	<?php break;
	case "close":
	?>

	</table><br />

	<?php break;
	case "title":
	?>
	<table width="100%" border="0" style="background-color:#dceefc; padding:5px 10px;"><tr>
	<td valign="top" colspan="2"><h3 style="font-family:Georgia,'Times New Roman',Times,serif;"><?php echo $value['name']; ?></h3></td>
	</tr>

	<!--custom-->


	<?php break;
	case "sub-title":
	?>
	<h3 style="font-family:Georgia,'Times New Roman',Times,serif; padding-left:8px;"><?php echo $value['name']; ?></h3>
	<!--end-of-custom-->


	<?php break;
	case 'text':
	?>

	<tr>
	<td valign="top" width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
	<td width="80%"><input style="width:400px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?>" /></td>
	</tr>

	<tr>
	<td><small><?php echo $value['desc']; ?></small></td>
	</tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

	<?php
	break;
	case 'textarea':
	?>

	<tr>
	<td valign="top" width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
	<td width="80%"><textarea name="<?php echo $value['id']; ?>" style="width:400px; height:200px;" type="<?php echo $value['type']; ?>" cols="" rows=""><?php if ( get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?></textarea></td>

	</tr>

	<tr>
	<td><small><?php echo $value['desc']; ?></small></td>
	</tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

	<?php
	break;
	case 'select':
	?>
	<tr>
	<td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
	<td width="80%"><select style="width:240px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"><?php foreach ($value['options'] as $option) { ?><option<?php if ( get_settings( $value['id'] ) == $option) { echo ' selected="selected"'; } elseif ($option == $value['std']) { echo ' selected="selected"'; } ?>><?php echo $option; ?></option><?php } ?></select></td>
	</tr>

	<tr>
	<td><small><?php echo $value['desc']; ?></small></td>
	</tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

	<?php
	break;
	case "checkbox":
	?>
	<tr>
	<td width="20%" rowspan="2" valign="middle"><strong><?php echo $value['name']; ?></strong></td>
	<td width="80%"><?php if(get_option($value['id'])){ $checked = "checked='checked"; }else{ $checked = "";} ?>
	<input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
	</td>
	</tr>

	<tr>
	<td><small><?php echo $value['desc']; ?></small></td>
	</tr><tr><td colspan="2" style="margin-bottom:5px;border-bottom:1px dotted #000000;">&nbsp;</td></tr><tr><td colspan="2">&nbsp;</td></tr>

	<?php break;
	}
	}
	?>

	<p class="submit">
	<input name="save" type="submit" value="Save changes" />
	<input type="hidden" name="action" value="save" />
	</p>
	</form>
	<form method="post">
	<p class="submit">
	<input name="reset" type="submit" value="Reset" />
	<input type="hidden" name="action" value="reset" />
	</p>
	</form>

	<?php
}
add_action('admin_menu', 'mytheme_add_admin');

function printshop_header_slider() {
	global $post, $wp_query;
	if (class_exists('WooSlider')) {
		$slidegroup = '';
		$slideshow_options = get_option('slideshow_options');
		if (is_front_page()) {
			$slidegroup = apply_filters( 'printshop_homepage_slider', $slideshow_options['page-home'] );
		} else if (is_page()) {
			$slidegroup = $slideshow_options['page-'.$post->ID];
		} else if (is_category() || is_tax()) {
			$term_id = $wp_query->get_queried_object_id();
			$slidegroup = $slideshow_options['cat-'.$term_id];
		}
		if ($slidegroup) {
			echo do_shortcode('[wooslider slide_page="'.$slidegroup.'" slider_type="slides" limit="20"  display_content="false" imageslide="true" order="DESC" order_by="date" size="full" control_nav="false"]');
		}
	}
}

remove_action('wp_head', 'printshop_print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'printshop_print_emoji_styles');

function printshop_print_emoji_detection_script() {
	?>
	<script type="text/javascript">
	window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/URL-of-your-website\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.2.1"}};
    !function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f;c.supports={simple:d("simple"),flag:d("flag")},c.supports.simple&&c.supports.flag||(f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
	</script>
	<?php
}

function printshop_print_emoji_styles() {
	?>
	<style type="text/css">
	img.wp-smiley,
	img.emoji {
		display: inline !important;
		border: none !important;
		box-shadow: none !important;
		height: 1em !important;
		width: 1em !important;
		margin: 0 .07em !important;
		vertical-align: -0.1em !important;
		background: none !important;
		padding: 0 !important;
	}
	</style>
	<?php
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// Classes
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

/**
 * Storefront_Top_Bar Class
 * Returns the main instance of Storefront_Top_Bar to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object Storefront_Top_Bar
 */

function Storefront_Top_Bar() {

	return Storefront_Top_Bar::instance();

} // End Storefront_Top_Bar()



Storefront_Top_Bar();



/**

 * Main Storefront_Top_Bar Class

 *

 * @class Storefront_Top_Bar

 * @version	1.0.0

 * @since 1.0.0

 * @package	Storefront_Top_Bar

 */

final class Storefront_Top_Bar {

	/**

	 * Storefront_Top_Bar The single instance of Storefront_Top_Bar.

	 * @var 	object

	 * @access  private

	 * @since 	1.0.0

	 */

	private static $_instance = null;



	/**

	 * The token.

	 * @var     string

	 * @access  public

	 * @since   1.0.0

	 */

	public $token;



	/**

	 * The version number.

	 * @var     string

	 * @access  public

	 * @since   1.0.0

	 */

	public $version;



	// Admin - Start

	/**

	 * The admin object.

	 * @var     object

	 * @access  public

	 * @since   1.0.0

	 */

	public $admin;



	/**

	 * Constructor function.

	 * @access  public

	 * @since   1.0.0

	 * @return  void

	 */

	public function __construct() {

		$this->token 			= 'storefront-top-bar';

		$this->plugin_url 		= plugin_dir_url( __FILE__ );

		$this->plugin_path 		= plugin_dir_path( __FILE__ );

		$this->version 			= '1.0.0';



		register_activation_hook( __FILE__, array( $this, 'install' ) );



		add_action( 'init', array( $this, 'woa_sf_setup' ) );



		add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'woa_sf_plugin_links' ) );

	}



	/**

	 * Main Storefront_Top_Bar Instance

	 *

	 * Ensures only one instance of Storefront_Top_Bar is loaded or can be loaded.

	 *

	 * @since 1.0.0

	 * @static

	 * @see Storefront_Top_Bar()

	 * @return Main Storefront_Top_Bar instance

	 */

	public static function instance() {

		if ( is_null( self::$_instance ) )

			self::$_instance = new self();

		return self::$_instance;

	} // End instance()




	/**

	 * Cloning is forbidden.

	 *

	 * @since 1.0.0

	 */

	public function __clone() {

		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '1.0.0' );

	}



	/**

	 * Unserializing instances of this class is forbidden.

	 *

	 * @since 1.0.0

	 */

	public function __wakeup() {

		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '1.0.0' );

	}



	/**

	 * Plugin page links

	 *

	 * @since  1.0.0

	 */

	public function woa_sf_plugin_links( $links ) {

		$plugin_links = array(

			'<a href="http://support.woothemes.com/">' . __( 'Support', 'printshop' ) . '</a>',

			'<a href="http://docs.woothemes.com/document/storefront-top-bar/">' . __( 'Docs', 'printshop' ) . '</a>',

		);



		return array_merge( $plugin_links, $links );

	}



	/**

	 * Installation.

	 * Runs on activation. Logs the version number and assigns a notice message to a WordPress option.

	 * @access  public

	 * @since   1.0.0

	 * @return  void

	 */

	public function install() {

		$this->_log_version_number();



		if( 'printshop' != basename( TEMPLATEPATH ) ) {

			deactivate_plugins( plugin_basename( __FILE__ ) );

			wp_die( 'Sorry, you can&rsquo;t activate this plugin unless you have installed the Storefront theme.' );

		}



		// get theme customizer url

		$url = admin_url() . 'customize.php?';

		$url .= 'url=' . urlencode( site_url() . '?storefront-customizer=true' ) ;

		$url .= '&return=' . urlencode( admin_url() . 'plugins.php' );

		$url .= '&storefront-customizer=true';



		$notices 		= get_option( 'woa_sf_activation_notice', array() );

		$notices[]		= sprintf( __( '%sThanks for installing the Storefront Top Bar extension. To get started, visit the %sCustomizer%s.%s %sOpen the Customizer%s', 'printshop' ), '<p>', '<a href="' . esc_url( $url ) . '">', '</a>', '</p>', '<p><a href="' . esc_url( $url ) . '" class="button button-primary">', '</a></p>' );



		update_option( 'woa_sf_activation_notice', $notices );

	}



	/**

	 * Log the plugin version number.

	 * @access  private

	 * @since   1.0.0

	 * @return  void

	 */

	private function _log_version_number() {

		// Log the version number.

		update_option( $this->token . '-version', $this->version );

	}



	/**

	 * Setup all the things.

	 * Only executes if Storefront or a child theme using Storefront as a parent is active and the extension specific filter returns true.

	 * Child themes can disable this extension using the storefront_top_bar_enabled filter

	 * @return void

	 */

	public function woa_sf_setup() {

		$theme = wp_get_theme();



		if ( 'printshop' == $theme->name || 'printshop' == $theme->template && apply_filters( 'storefront_top_bar_supported', true ) ) {

			add_action( 'customize_register', array( $this, 'woa_sf_customize_register' ) );

			add_filter( 'body_class', array( $this, 'woa_sf_body_class' ) );

			add_action( 'storefront_before_header', array( $this, 'woa_sf_layout_adjustments' ) );

			add_action( 'admin_notices', array( $this, 'woa_sf_customizer_notice' ) );

			add_action( 'wp_head', array( $this, 'inline_css') );



			// Hide the 'More' section in the customizer

			add_filter( 'storefront_customizer_more', '__return_false' );

		}

	}



	/**

	 * Admin notice

	 * Checks the notice setup in install(). If it exists display it then delete the option so it's not displayed again.

	 * @since   1.0.0

	 * @return  void

	 */

	public function woa_sf_customizer_notice() {

		$notices = get_option( 'woa_sf_activation_notice' );



		if ( $notices = get_option( 'woa_sf_activation_notice' ) ) {



			foreach ( $notices as $notice ) {

				echo '<div class="updated">' . $notice . '</div>';

			}



			delete_option( 'woa_sf_activation_notice' );

		}

	}



	/**

	 * Customizer Controls and settings

	 * @param WP_Customize_Manager $wp_customize Theme Customizer object.

	 */

	public function woa_sf_customize_register( $wp_customize ) {

		/**
		 * Add new section
		 */
		$wp_customize->add_section( 
			'woa_sf_top_bar' , array(
			    'title'      => __( 'Top Bar', 'wooassist' ),
			    'priority'   => 30,
			)
		);

		/**
		 * Add new settings
		 */
		$wp_customize->add_setting( 
			'woa_sf_topbar_bgcolor',
			array(
				'default' => apply_filters( 'woa_sf_topbar_default_bgcolor', '#5b5b5b' ),
				'sanitize_callback' => 'sanitize_hex_color',
			)
		);

		$wp_customize->add_setting( 
			'woa_sf_topbar_txtcolor',
			array(
				'default' => apply_filters( 'woa_sf_topbar_default_txtcolor', '#efefef' ),
				'sanitize_callback' => 'sanitize_hex_color',
			)
		);

		$wp_customize->add_setting( 
			'woa_sf_topbar_linkcolor',
			array(
				'default' => apply_filters( 'woa_sf_topbar_default_linkcolor', '#ffffff' ),
				'sanitize_callback' => 'sanitize_hex_color',
			)
		);

		$wp_customize->add_setting( 
			'woa_sf_topbar_mobile_display',
			array(
				'default' => 'show-on-mobile',
			)
		);

		/**
		 * Add controls and apply respective settings and hook on section
		 */
		$wp_customize->add_control( 
			new WP_Customize_Color_Control( 
			$wp_customize, 
			'woa_sf_topbar_bgcolor', 
			array(
				'label'      => __( 'Background Color', 'mytheme' ),
				'section'    => 'woa_sf_top_bar',
				'settings'   => 'woa_sf_topbar_bgcolor',
			) ) 
		);

		$wp_customize->add_control( 
			new WP_Customize_Color_Control( 
			$wp_customize, 
			'woa_sf_topbar_txtcolor', 
			array(
				'label'      => __( 'Text Color', 'mytheme' ),
				'section'    => 'woa_sf_top_bar',
				'settings'   => 'woa_sf_topbar_txtcolor',
			) ) 
		);

		$wp_customize->add_control( 
			new WP_Customize_Color_Control( 
			$wp_customize, 
			'woa_sf_topbar_linkcolor', 
			array(
				'label'      => __( 'Link Color', 'mytheme' ),
				'section'    => 'woa_sf_top_bar',
				'settings'   => 'woa_sf_topbar_linkcolor',
			) ) 
		);

		$wp_customize->add_control(
		    new WP_Customize_Control(
		        $wp_customize,
		        'woa_sf_topbar_mobile_display',
		        array(
		            'label'          => __( 'Mobile Display', 'wooassist' ),
		            'section'        => 'woa_sf_top_bar',
		            'settings'       => 'woa_sf_topbar_mobile_display',
		            'type'           => 'radio',
		            'choices'        => array(
		                'show-on-mobile'   => __( 'Show' ),
		                'hide-on-mobile'  => __( 'Hide' )
		            )
		        )
		    )
		);
	}


	/**

	 * Adjust hex color brightness

	 * @param $hex $steps

	 */

	function adjust_brightness($hex, $steps) {

	    // Steps should be between -255 and 255. Negative = darker, positive = lighter

	    $steps = max(-255, min(255, $steps));



	    // Normalize into a six character long hex string

	    $hex = str_replace('#', '', $hex);

	    if (strlen($hex) == 3) {

	        $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);

	    }



	    // Split into three parts: R, G and B

	    $color_parts = str_split($hex, 2);

	    $return = '#';



	    foreach ($color_parts as $color) {

	        $color   = hexdec($color); // Convert to decimal

	        $color   = max(0,min(255,$color + $steps)); // Adjust color

	        $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code

	    }



	    return $return;

	}



	/**

	 * Inline CSS

	 */

	function inline_css() {



		$bg_color		= 	get_theme_mod( 'woa_sf_topbar_bgcolor', apply_filters( 'woa_sf_topbar_default_bgcolor', '#5b5b5b' ) );

		$submenu_bg		=	$this->adjust_brightness( $bg_color, -25 );

		$txt_color		= 	get_theme_mod( 'woa_sf_topbar_txtcolor', apply_filters( 'woa_sf_topbar_default_txtcolor', '#efefef' ) );

		$link_color		=	get_theme_mod( 'woa_sf_topbar_linkcolor', apply_filters( 'woa_sf_topbar_default_linkcolor', '#ffffff' ) );



		?>

		<style type="text/css">

			.woa-top-bar-wrap, .woa-top-bar .block .widget_nav_menu ul li .sub-menu { background: <?php echo $bg_color; ?>; } .woa-top-bar .block .widget_nav_menu ul li .sub-menu li a:hover { background: <?php echo $submenu_bg; ?> } .woa-top-bar-wrap * { color: <?php echo $txt_color; ?>; } .woa-top-bar-wrap a, .woa-top-bar-wrap .widget_nav_menu li.current-menu-item > a { color: <?php echo $link_color; ?> !important; } .woa-top-bar-wrap a:hover { opacity: 0.9; }

		</style>

		<?php

	}



	/**

	 * Storefront Top Bar Body Class

	 * Adds a class based on the extension name and any relevant settings.

	 */

	public function woa_sf_body_class( $classes ) {

		$classes[] = 'storefront-top-bar-active';



		return $classes;

	}



	/**

	 * Layout

	 * Adjusts the default Storefront layout when the plugin is active

	 */

	public function woa_sf_layout_adjustments() {

		

		if ( is_active_sidebar( 'woa-top-bar-2' ) ) {

			$widget_columns = apply_filters( 'woa_top_widget_regions', 2 );

		} elseif ( is_active_sidebar( 'woa-top-bar-1' ) ) {

			$widget_columns = apply_filters( 'woa_top_widget_regions', 1 );

		} else {

			$widget_columns = apply_filters( 'woa_top_widget_regions', 0 );

		}



		$mobile_toggle = get_theme_mod( 'woa_sf_topbar_mobile_display', 'show-on-mobile' );



		if ( $widget_columns > 0 ) :

			$site_header_style = apply_filters( 'printshop_background_color', $site_header_style, 'topbar' ); ?>

			<div class="woa-top-bar-wrap<?php echo ' ' . $mobile_toggle; ?>"<?php if ( $site_header_style ) { echo 'style="'.$site_header_style.'"'; } ?>>



				<div class="col-full">



					<section class="woa-top-bar col-<?php echo intval( $widget_columns ); ?> fix">



						<?php $i = 0; while ( $i < $widget_columns ) : $i++; ?>



							<?php if ( is_active_sidebar( 'woa-top-bar-' . $i ) ) : ?>



								<section class="block woa-top-bar-<?php echo intval( $i ); ?>">

						        	<?php dynamic_sidebar( 'woa-top-bar-' . intval( $i ) ); ?>

								</section>



					        <?php endif; ?>



						<?php endwhile; ?>



						<div class="clear"></div>



					</section>



				</div>



			</div>



		<?php endif;

	}


} // End Class


/**
 * Returns the main instance of Storefront_Site_Logo to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object Storefront_Site_Logo
 */

function Storefront_Site_Logo() {

	return Storefront_Site_Logo::instance();

} // End Storefront_Site_Logo()



Storefront_Site_Logo();



/**

 * Main Storefront_Site_Logo Class

 *

 * @class Storefront_Site_Logo

 * @version	1.0.0

 * @since 1.0.0

 * @package	Storefront_Site_Logo

 */

final class Storefront_Site_Logo {

	/**

	 * Storefront_Site_Logo The single instance of Storefront_Site_Logo.

	 * @var 	object

	 * @access  private

	 * @since 	1.0.0

	 */

	private static $_instance = null;



	/**

	 * The token.

	 * @var     string

	 * @access  public

	 * @since   1.0.0

	 */

	public $token;



	/**

	 * The version number.

	 * @var     string

	 * @access  public

	 * @since   1.0.0

	 */

	public $version;



	// Admin - Start

	/**

	 * The admin object.

	 * @var     object

	 * @access  public

	 * @since   1.0.0

	 */

	public $admin;



	/**

	 * Constructor function.

	 * @access  public

	 * @since   1.0.0

	 * @return  void

	 */

	public function __construct() {

		$this->token 			= 'storefront-site-logo';

		$this->plugin_url 		= plugin_dir_url( __FILE__ );

		$this->plugin_path 		= plugin_dir_path( __FILE__ );

		$this->version 			= '1.2.0';



		register_activation_hook( __FILE__, array( $this, 'install' ) );



		add_action( 'init', array( $this, 'woa_sf_setup' ) );



		add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'woa_sf_plugin_links' ) );

	}



	/**

	 * Main Storefront_Site_Logo Instance

	 *

	 * Ensures only one instance of Storefront_Site_Logo is loaded or can be loaded.

	 *

	 * @since 1.0.0

	 * @static

	 * @see Storefront_Site_Logo()

	 * @return Main Storefront_Site_Logo instance

	 */

	public static function instance() {

		if ( is_null( self::$_instance ) )

			self::$_instance = new self();

		return self::$_instance;

	} // End instance()




	/**

	 * Cloning is forbidden.

	 *

	 * @since 1.0.0

	 */

	public function __clone() {

		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '1.0.0' );

	}



	/**

	 * Unserializing instances of this class is forbidden.

	 *

	 * @since 1.0.0

	 */

	public function __wakeup() {

		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '1.0.0' );

	}



	/**

	 * Plugin page links

	 *

	 * @since  1.0.0

	 */

	public function woa_sf_plugin_links( $links ) {

		$plugin_links = array(

			'<a href="https://wordpress.org/support/plugin/storefront-site-logo">' . __( 'Support', 'printshop' ) . '</a>',

		);



		return array_merge( $plugin_links, $links );

	}



	/**

	 * Installation.

	 * Runs on activation. Logs the version number and assigns a notice message to a WordPress option.

	 * @access  public

	 * @since   1.0.0

	 * @return  void

	 */

	public function install() {

		$this->_log_version_number();



		if( 'printshop' != basename( TEMPLATEPATH ) ) {

			deactivate_plugins( plugin_basename( __FILE__ ) );

			wp_die( 'Sorry, you can&rsquo;t activate this plugin unless you have installed the Storefront theme.' );

		}



		// get theme customizer url

		$url = admin_url() . 'customize.php?';

		$url .= 'url=' . urlencode( site_url() . '?storefront-customizer=true' ) ;

		$url .= '&return=' . urlencode( admin_url() . 'plugins.php' );

		$url .= '&storefront-customizer=true';



		$notices 		= get_option( 'woa_sf_activation_notice', array() );

		$notices[]		= sprintf( __( '%sThanks for installing the Storefront Site Logo extension. To get started, visit the %sCustomizer%s.%s %sOpen the Customizer%s', 'printshop' ), '<p>', '<a href="' . esc_url( $url ) . '">', '</a>', '</p>', '<p><a href="' . esc_url( $url ) . '" class="button button-primary">', '</a></p>' );



		update_option( 'woa_sf_activation_notice', $notices );

	}



	/**

	 * Log the plugin version number.

	 * @access  private

	 * @since   1.0.0

	 * @return  void

	 */

	private function _log_version_number() {

		// Log the version number.

		update_option( $this->token . '-version', $this->version );

	}



	/**

	 * Setup all the things.

	 * Only executes if Storefront or a child theme using Storefront as a parent is active and the extension specific filter returns true.

	 * Child themes can disable this extension using the storefront_site_logo_enabled filter

	 * @return void

	 */

	public function woa_sf_setup() {

		$theme = wp_get_theme();



		if ( 'printshop' == $theme->name || 'printshop' == $theme->template && apply_filters( 'storefront_site_logo_supported', true ) ) {

			add_action( 'customize_register', array( $this, 'woa_sf_customize_register') );

			add_filter( 'body_class', array( $this, 'woa_sf_body_class' ) );

			add_action( 'admin_notices', array( $this, 'woa_sf_customizer_notice' ) );



			$header_layout = function_exists( 'Storefront_Designer' ) ? get_theme_mod( 'sd_header_layout', 'compact' ) : 'compact';



			// replace default branding function

			if ( $header_layout == 'expanded' ) { // check if Storefront Designer plugin is installed

				remove_action( 'storefront_header', 'storefront_site_branding', 45 );

				add_action( 'storefront_header', array( $this, 'woa_sf_layout_adjustments' ), 45 );

			} else {

				remove_action( 'storefront_header', 'storefront_site_branding', 20 );

				add_action( 'storefront_header', array( $this, 'woa_sf_layout_adjustments' ), 20 );

			}



			// Hide the 'More' section in the customizer

			add_filter( 'storefront_customizer_more', '__return_false' );

		}

	}



	/**

	 * Admin notice

	 * Checks the notice setup in install(). If it exists display it then delete the option so it's not displayed again.

	 * @since   1.0.0

	 * @return  void

	 */

	public function woa_sf_customizer_notice() {

		$notices = get_option( 'woa_sf_activation_notice' );



		if ( $notices = get_option( 'woa_sf_activation_notice' ) ) {



			foreach ( $notices as $notice ) {

				echo '<div class="updated">' . $notice . '</div>';

			}



			delete_option( 'woa_sf_activation_notice' );

		}

	}



	/**

	 * Customizer Controls and settings

	 * @param WP_Customize_Manager $wp_customize Theme Customizer object.

	 */

	public function woa_sf_customize_register( $wp_customize ) {



		/**

		 * Add new section

		 */

		$wp_customize->add_section(

			'woa_sf_branding' , array(

			    'title'      => __( 'Branding', 'printshop' ),

			    'priority'   => 30,

			)

		);



		/**

		 * Add new settings

		 */

		$wp_customize->add_setting( 'woa_sf_enable_logo' );

		$wp_customize->add_setting( 'woa_sf_logo' );



		/**

		 * Add new controls and assigning the settings and it's section

		 */

		$wp_customize->add_control(

	        new WP_Customize_Control(

	            $wp_customize,

	            'woa_sf_enable_logo_img',

	            array(

	                'label'      => __( 'Choose branding style', 'printshop' ),

	                'section'    => 'woa_sf_branding',

	                'settings'   => 'woa_sf_enable_logo',

	                'type'		 => 'radio',

	                'choices'	 => array(

	                	'title_tagline'		=>	__( 'Title and Tagline', 'printshop' ),

	                	'logo_img'			=>	__( 'Logo image', 'printshop' ),

						'logo_img_tagline'	=>	__( 'Logo image and Tagline', 'printshop' )

	                )

	            )

	        )

	    );



		$wp_customize->add_control(

	        new WP_Customize_Image_Control(

	            $wp_customize,

	            'woa_sf_logo_img',

	            array(

	                'label'      => __( 'Logo Image', 'printshop' ),

	                'section'    => 'woa_sf_branding',

	                'settings'   => 'woa_sf_logo'

	            )

	        )

	    );

	}



	/**

	 * Storefront Site Logo Body Class

	 * Adds a class based on the extension name and any relevant settings.

	 */

	public function woa_sf_body_class( $classes ) {
		$classes[] = 'storefront-site-logo-active';
		return $classes;

	}



	/**

	 * Layout

	 * Adjusts the default Storefront layout when the plugin is active

	 */

	public function woa_sf_layout_adjustments() {



		$check = get_theme_mod( 'woa_sf_enable_logo', 'title_tagline' );

		$logo = get_theme_mod( 'woa_sf_logo', null );

		$logo = apply_filters( 'printshop_site_logo', $logo );

		$home_url = home_url('/');

		$home_url = apply_filters( 'printshop_homepage_url', $home_url );

		if( ( $check == 'logo_img' || $check == 'logo_img_tagline' ) && $logo ) {



			if( is_ssl() ) {

				$logo = str_replace( 'http://', 'https://', $logo );
				$home_url = str_replace( 'http://', 'https://', $home_url );

			}



			?>

			<div class="site-branding site-logo-anchor">

				<a href="<?php echo $home_url; ?>">

					<img src="<?php echo $logo; ?>" style="display:inline-block;">

				</a>

				<?php if ( $check == 'logo_img_tagline' ) : ?>

					<p class="site-description"><?php bloginfo( 'description' ); ?></p>

				<?php endif; ?>

			</div>

		<?php

		}

		else if ( function_exists( 'jetpack_has_site_logo' ) && jetpack_has_site_logo() ) {

			jetpack_the_site_logo();

		} else { ?>

			<div class="site-branding">

				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>

				<p class="site-description"><?php bloginfo( 'description' ); ?></p>

			</div>

		<?php }

	}



} // End Class
?>